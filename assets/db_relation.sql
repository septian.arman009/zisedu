-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 24, 2019 at 04:47 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ci_bimbel`
--

-- --------------------------------------------------------

--
-- Table structure for table `bills`
--

CREATE TABLE `bills` (
  `id` int(10) NOT NULL,
  `b_code` varchar(20) NOT NULL,
  `m_code` varchar(20) NOT NULL,
  `period` int(4) NOT NULL,
  `payment` text NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `id` int(10) NOT NULL,
  `c_code` varchar(20) NOT NULL,
  `name` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `c_matrixs`
--

CREATE TABLE `c_matrixs` (
  `id` int(10) NOT NULL,
  `c_code` varchar(20) NOT NULL,
  `t_code` varchar(20) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `c_prices`
--

CREATE TABLE `c_prices` (
  `id` int(10) NOT NULL,
  `t_code` varchar(20) NOT NULL,
  `c_code` varchar(20) NOT NULL,
  `edu` varchar(30) NOT NULL,
  `edu_level` varchar(15) NOT NULL,
  `price` int(10) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE `logs` (
  `id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `name` varchar(50) NOT NULL,
  `log` text NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE `members` (
  `id` int(10) NOT NULL,
  `m_code` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `name` varchar(50) NOT NULL,
  `address` text NOT NULL,
  `edu` varchar(30) NOT NULL,
  `edu_place` varchar(100) NOT NULL,
  `edu_level` varchar(15) NOT NULL,
  `join_date` date NOT NULL,
  `status` int(1) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`id`, `m_code`, `email`, `name`, `address`, `edu`, `edu_place`, `edu_level`, `join_date`, `status`, `created_at`) VALUES
(1, 'M-06190001', 'ariel@gmail.com', 'Arman', 'Agus Salim', 'Primary School', 'Persada', '5', '2019-06-24', 1, '2019-06-24 21:34:43');

-- --------------------------------------------------------

--
-- Table structure for table `m_courses`
--

CREATE TABLE `m_courses` (
  `id` int(10) NOT NULL,
  `mc_code` varchar(25) NOT NULL,
  `m_code` varchar(20) NOT NULL,
  `course` text,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `role_id` int(10) NOT NULL,
  `name` varchar(30) NOT NULL,
  `display_name` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`role_id`, `name`, `display_name`, `created_at`) VALUES
(1, 'admin', 'Admin', '2019-06-20 19:48:03'),
(2, 'user', 'User', '2019-06-20 19:48:03'),
(3, 'owner', 'Owner', '2019-06-06 23:06:25');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) NOT NULL,
  `max_member` int(10) NOT NULL,
  `max_payment_date` int(10) NOT NULL,
  `protocol` varchar(30) NOT NULL,
  `smtp_host` varchar(30) NOT NULL,
  `smtp_port` int(10) NOT NULL,
  `send_mail` varchar(50) NOT NULL,
  `send_pass` varchar(30) NOT NULL,
  `email` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `max_member`, `max_payment_date`, `protocol`, `smtp_host`, `smtp_port`, `send_mail`, `send_pass`, `email`) VALUES
(1, 10, 10, 'smtp', 'ssl://smtp.gmail.com', 465, 'alternate.septian@gmail.com', 'januari1993', 'on');

-- --------------------------------------------------------

--
-- Table structure for table `teachers`
--

CREATE TABLE `teachers` (
  `id` int(10) NOT NULL,
  `t_code` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `name` varchar(50) NOT NULL,
  `address` text NOT NULL,
  `phone` varchar(13) NOT NULL,
  `degree` varchar(100) NOT NULL,
  `status` int(1) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `token_passwords`
--

CREATE TABLE `token_passwords` (
  `id` int(10) NOT NULL,
  `email` varchar(50) NOT NULL,
  `token` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(10) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `role_id` int(1) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `name`, `email`, `password`, `role_id`, `created_at`) VALUES
(1, 'Admin', 'admin@gmail.com', '5456db00ec97635f7d998e3290a01b6d', 1, '2018-05-25 00:00:00'),
(2, 'Septian', 'septian.arman009@gmail.com', 'edb0fff189d39f93ed1778fa0ce0a0a9', 2, '2019-06-24 21:31:54'),
(3, 'Alternate', 'alternate.septian@gmail.com', '921fa9a7bd6ca4aecc8e18c43c138b65', 1, '2019-06-24 21:33:08');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bills`
--
ALTER TABLE `bills`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `b_code` (`b_code`),
  ADD KEY `m_code` (`m_code`);

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `c_code` (`c_code`);

--
-- Indexes for table `c_matrixs`
--
ALTER TABLE `c_matrixs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `c_code` (`c_code`,`t_code`),
  ADD KEY `t_code` (`t_code`);

--
-- Indexes for table `c_prices`
--
ALTER TABLE `c_prices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `t_code` (`t_code`,`c_code`),
  ADD KEY `c_code` (`c_code`);

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `members`
--
ALTER TABLE `members`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `m_code` (`m_code`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `m_courses`
--
ALTER TABLE `m_courses`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `mc_code` (`mc_code`),
  ADD KEY `m_code` (`m_code`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`role_id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teachers`
--
ALTER TABLE `teachers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `t_code` (`t_code`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `token_passwords`
--
ALTER TABLE `token_passwords`
  ADD PRIMARY KEY (`id`),
  ADD KEY `email` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `role_id` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bills`
--
ALTER TABLE `bills`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `c_matrixs`
--
ALTER TABLE `c_matrixs`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `c_prices`
--
ALTER TABLE `c_prices`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `logs`
--
ALTER TABLE `logs`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `members`
--
ALTER TABLE `members`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `m_courses`
--
ALTER TABLE `m_courses`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `role_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `teachers`
--
ALTER TABLE `teachers`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `token_passwords`
--
ALTER TABLE `token_passwords`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `bills`
--
ALTER TABLE `bills`
  ADD CONSTRAINT `bills_ibfk_1` FOREIGN KEY (`m_code`) REFERENCES `members` (`m_code`);

--
-- Constraints for table `c_matrixs`
--
ALTER TABLE `c_matrixs`
  ADD CONSTRAINT `c_matrixs_ibfk_1` FOREIGN KEY (`t_code`) REFERENCES `teachers` (`t_code`),
  ADD CONSTRAINT `c_matrixs_ibfk_2` FOREIGN KEY (`c_code`) REFERENCES `courses` (`c_code`);

--
-- Constraints for table `c_prices`
--
ALTER TABLE `c_prices`
  ADD CONSTRAINT `c_prices_ibfk_1` FOREIGN KEY (`c_code`) REFERENCES `courses` (`c_code`),
  ADD CONSTRAINT `c_prices_ibfk_2` FOREIGN KEY (`t_code`) REFERENCES `teachers` (`t_code`);

--
-- Constraints for table `logs`
--
ALTER TABLE `logs`
  ADD CONSTRAINT `logs_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`);

--
-- Constraints for table `m_courses`
--
ALTER TABLE `m_courses`
  ADD CONSTRAINT `m_courses_ibfk_1` FOREIGN KEY (`m_code`) REFERENCES `members` (`m_code`);

--
-- Constraints for table `token_passwords`
--
ALTER TABLE `token_passwords`
  ADD CONSTRAINT `token_passwords_ibfk_1` FOREIGN KEY (`email`) REFERENCES `users` (`email`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `roles` (`role_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
