function start_page() {
	var bim_url = document.cookie.replace(/(?:(?:^|.*;\s*)bim_url\s*\=\s*([^;]*).*$)|^.*$/, "$1");
	if (bim_url) {
		var main_id = document.cookie.replace(/(?:(?:^|.*;\s*)main_id\s*\=\s*([^;]*).*$)|^.*$/, "$1");
		if (main_id) {
			var main_id = document.cookie.replace(/(?:(?:^|.*;\s*)main_id\s*\=\s*([^;]*).*$)|^.*$/, "$1");
			main_menu(main_id, bim_url);
		} else {
			var main_sub_id = document.cookie.replace(/(?:(?:^|.*;\s*)main_sub_id\s*\=\s*([^;]*).*$)|^.*$/, "$1");
			if (main_sub_id) {
				var sub_id = document.cookie.replace(/(?:(?:^|.*;\s*)sub_id\s*\=\s*([^;]*).*$)|^.*$/, "$1");
                sub_menu(main_sub_id, sub_id, bim_url);
                var toggle = main_sub_id + '_toggle';
                $(toggle).click();
			}
		}
	} else {
		main_menu('#home', 'main_controller/home');
		$("#side_img").show();
		$("#ajax-loading").hide();
	}
}

function main_menu(main_id, bim_url) {
	var main_id_old = document.cookie.replace(/(?:(?:^|.*;\s*)main_id\s*\=\s*([^;]*).*$)|^.*$/, "$1");
	$(main_id_old).removeClass("active");

	var main_sub_id = document.cookie.replace(/(?:(?:^|.*;\s*)main_sub_id\s*\=\s*([^;]*).*$)|^.*$/, "$1");
	$(main_sub_id).removeClass("active");

	var sub_id = document.cookie.replace(/(?:(?:^|.*;\s*)sub_id\s*\=\s*([^;]*).*$)|^.*$/, "$1");
	$(sub_id).removeClass("active_c");

    $(main_id).addClass("active");
    
    var toggle = main_sub_id + '_toggle';
    $(toggle).click();

	document.cookie = "main_id=" + main_id;
	document.cookie = "main_sub_id=";
	document.cookie = "sub_id=";
	document.cookie = "bim_url=" + bim_url;

	loadViewMain(bim_url, '.content');
}

function sub_menu(main_sub_id, sub_id, bim_url) {
	var main_id = document.cookie.replace(/(?:(?:^|.*;\s*)main_id\s*\=\s*([^;]*).*$)|^.*$/, "$1");
	$(main_id).removeClass("active");

	var main_sub_id_old = document.cookie.replace(/(?:(?:^|.*;\s*)main_sub_id\s*\=\s*([^;]*).*$)|^.*$/, "$1");
	$(main_sub_id_old).removeClass("active");

	var sub_id_old = document.cookie.replace(/(?:(?:^|.*;\s*)sub_id\s*\=\s*([^;]*).*$)|^.*$/, "$1");
	$(sub_id_old).removeClass("active_c");

	$(main_sub_id).addClass("active");
    $(sub_id).addClass("active_c");
    
	document.cookie = "main_id=";
	document.cookie = "main_sub_id=" + main_sub_id;
	document.cookie = "sub_id=" + sub_id;
	document.cookie = "bim_url=" + bim_url;

	loadViewMain(bim_url, '.content');
}

function loadViewMain(url, div) {
	$.ajax({
		url: url,
		beforeSend: function () {
			$("#side_img").hide();
			$("#ajax-loading").show();
		},
		success: function (data) {
			$(div).html(data);
			$("#side_img").show();
			$("#ajax-loading").hide();
			$(div).html(data);
		}
	});
}

function loadView(url, div) {
	$.ajax({
		url: url,
		beforeSend: function () {
			$("#side_img").hide();
			$("#ajax-loading").show();
		},
		success: function (data) {
			document.cookie = "bim_url=" + url;
			$(div).html(data);
			$("#side_img").show();
			$("#ajax-loading").hide();
			$(div).html(data);
		}
	});
}

function loadPartial(url, div) {
	$.ajax({
		url: url,
		beforeSend: function () {
			$("#side_img").hide();
			$("#ajax-loading").show();
		},
		success: function (data) {
			$(div).html(data);
			$("#side_img").show();
			$("#ajax-loading").hide();
		}
	});
}