function postData(url, data, callback) {
	$.ajax({
		type: "POST",
		contentType: "application/json",
		dataType: "json",
		url: url,
		data: JSON.stringify(data),
		success: function (response) {
			return callback(null, response);
		},
		error: function (err) {
			return callback(true, err);
		}
	});
}

function do_something(url) {
	$.ajax({
		type: "POST",
		url: url
	});
}

function _(id) {
	return document.getElementById(id);
}

function logout() {
	swal({
			title: "Are you sure want to logout ?",
			text: "",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-danger",
			confirmButtonText: "Yes, Logout Now ",
			closeOnConfirm: false
		},
		function () {
			do_something("main_controller/logout");
			setTimeout(() => {
				location.reload();
			}, 500);
		}
	);
}

function ValidateEmail(mail, btn, id_msg) {
	if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)) {
		$(id_msg).hide();
		return true;
	}
	$(id_msg).show();
	$(btn).attr("disabled", "disabled");
}

function setTheme(theme) {
	do_something("main_controller/setTheme/" + theme);
	setTimeout(function () {
		location.reload();
	}, 1000);
}

function to_date(date) {
	var temp = date.split('-');
	var d = new Date(temp[0] + '-' + temp[1] + '-' + temp[2]);
	if(temp[1] == '01'){
		var month = 'January';
	}else if(temp[1] == '02'){
		var month = 'February';
	}else if(temp[1] == '03'){
		var month = 'March';
	}else if(temp[1] == '04'){
		var month = 'April';
	}else if(temp[1] == '05'){
		var month = 'May';
	}else if(temp[1] == '06'){
		var month = 'June';
	}else if(temp[1] == '07'){
		var month = 'July';
	}else if(temp[1] == '08'){
		var month = 'August';
	}else if(temp[1] == '09'){
		var month = 'September';
	}else if(temp[1] == '10'){
		var month = 'October';
	}else if(temp[1] == '11'){
		var month = 'November';
	}else if(temp[1] == '12'){
		var month = 'December';
	}

	var d = temp[2]+' '+month+' '+temp[0];
	return d;
	
}

String.prototype.replaceAll = function (str1, str2, ignore) {
	return this.replace(new RegExp(str1.replace(/([\/\,\!\\\^\$\{\}\[\]\(\)\.\*\+\?\|\<\>\-\&])/g, "\\$&"), (ignore ? "gi" : "g")), (typeof (str2) == "string") ? str2.replace(/\$/g, "$$$$") : str2);
}