<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Main_model extends CI_Model
{

    public function gda1p($table)
    {
        return $this->db->get($table)->result_array();
    }

    public function gda3p($table, $column, $id)
    {
        $this->db->where($column, $id);
        return $this->db->get($table)->result_array();
    }

    public function gda5p($table, $column, $id, $column1, $id1)
    {
        $this->db->where($column, $id);
        $this->db->where($column1, $id1);
        return $this->db->get($table)->result_array();
    }

    public function gda5po($table, $column, $id, $column1, $id1, $order)
    {
        $this->db->where($column, $id);
        $this->db->where($column1, $id1);
        $this->db->order_by($order, 'ASC');
        return $this->db->get($table)->result_array();
    }

    public function gda7p($table, $column, $id, $column1, $id1, $column2, $id2)
    {
        $this->db->where($column, $id);
        $this->db->where($column1, $id1);
        $this->db->where($column2, $id2);
        return $this->db->get($table)->result_array();
    }

    public function gda9p($table, $column, $id, $column1, $id1, $column2, $id2, $column3, $id3)
    {
        $this->db->where($column, $id);
        $this->db->where($column1, $id1);
        $this->db->where($column2, $id2);
        $this->db->where($column3, $id3);
        return $this->db->get($table)->result_array();
    }

    public function gdo4p($table, $take, $column, $id)
    {
        $this->db->where($column, $id);
        $query = $this->db->get($table)->result_array();
        if($query){
            if($query[0][$take]){
                return $query[0][$take];
            }
        }
        
    }

    public function update($table, $data, $column, $id)
    {
        $this->db->where($column, $id);
        return $this->db->update($table, $data);
    }

    public function update6p($table, $data, $column, $id, $column1, $id1)
    {
        $this->db->where($column, $id);
        $this->db->where($column1, $id1);
        return $this->db->update($table, $data);
    }

    public function update8p($table, $data, $column, $id, $column1, $id1, $column2, $id2)
    {
        $this->db->where($column, $id);
        $this->db->where($column1, $id1);
        $this->db->where($column2, $id2);
        return $this->db->update($table, $data);
    }

    public function update10p($table, $data, $column, $id, $column1, $id1, $column2, $id2, $column3, $id3)
    {
        $this->db->where($column, $id);
        $this->db->where($column1, $id1);
        $this->db->where($column2, $id2);
        $this->db->where($column3, $id3);
        return $this->db->update($table, $data);
    }

    public function store($table, $data)
    {
        return $this->db->insert($table, $data);
    }

    public function truncate($table)
    {
        return $this->db->query("truncate $table");
    }

    public function destroy($table, $column, $id)
    {
        $this->db->where($column, $id);
        return $this->db->delete($table);
    }

    public function destroy2p($table, $column, $id, $column1, $id1)
    {
        $this->db->where($column, $id);
        $this->db->where($column1, $id1);
        return $this->db->delete($table);
    }

    public function max($tabel, $kolom)
    {
        $query = $this->db->query("select max($kolom)$kolom from $tabel")->result_array();
        return $query[0][$kolom];
    }

    public function last_where($table, $id, $column, $data)
    {
        $query = $this->db->query("select * from $table where $id = (select max($id) from $table where $column='$data')")->result_array();
        return $query;
    }

    public function max_all($tabel, $kolom)
    {
        $query = $this->db->query("select * from $tabel where $kolom=(select max($kolom) from $tabel)")->result_array();
        return $query;
    }

    public function count($table, $id)
    {
        $query = $this->db->query("select count($id) as total_row from $table")->result_array();
        if ($query[0]['total_row'] != '') {
            return $query[0]['total_row'];
        } else {
            return 0;
        }
    }

    public function count2where($table, $id, $column, $data, $column1, $data1)
    {
        $query = $this->db->query("select count($id) as total_row from $table
		where $column = '$data' and $column1 = '$data1'")->result_array();
        if ($query[0]['total_row'] != '') {
            return $query[0]['total_row'];
        } else {
            return 0;
        }
    }

    public function sum2where($tabel, $take, $column, $data, $column1, $data1)
    {
        $query = $this->db->query("select sum($take) as total from $tabel
		where $column = '$data' and $column1 = '$data1'")->result_array();
        if ($query[0]['total'] != '') {
            return $query[0]['total'];
        } else {
            return 0;
        }
    }

    public function sumwhere($tabel, $take, $column, $data)
    {
        $query = $this->db->query("select sum($take) as total from $tabel
		where $column = '$data'")->result_array();
        if ($query[0]['total'] != '') {
            return $query[0]['total'];
        } else {
            return 0;
        }
    }

    public function sumwhere_collect($tabel, $take, $column, $data, $id_collect)
    {
        $query = $this->db->query("select sum($take) as total from $tabel
		where $column = '$data'  and id in ($id_collect)")->result_array();
        if ($query[0]['total'] != '') {
            return $query[0]['total'];
        } else {
            return 0;
        }
    }

    public function pembayaran_masuk($id, $tahun, $bulan)
    {
        $id_murid = $this->gda3p('murid', 'pengemudi', $id);

        $id_collect = '';
        foreach ($id_murid as $key => $value) {
            if ($id_collect == '') {
                $id_collect = $value['id'];
            } else {
                $id_collect = $id_collect . ',' . $value['id'];
            }
        }

        $pembayaran_masuk = $this->db->query("select sum(nominal_bayar) as pembayaran_masuk from pembayaran where
		id_murid in ($id_collect) and tahun = '$tahun' and bulan = '$bulan'")->result_array();

        if ($$pembayaran_masuk[0]['pembayaran_masuk'] != '') {
            return $pembayaran_masuk[0]['pembayaran_masuk'];
        } else {
            return 0;
        }
    }

    public function in3p($table, $column, $in)
    {
        return $query = $this->db->query("select * from $table where $column in ($in)")->result_array();
    }

    public function sumin2where($tabel, $take, $column, $in, $column1, $data1, $column2, $data2)
    {
        $query = $this->db->query("select sum($take) as total from $tabel
		where $column in ($in) and $column1 = '$data1' and $column2 = '$data2'")->result_array();

        if ($query[0]['total'] != '') {
            return $query[0]['total'];
        } else {
            return 0;
        }
    }

    public function in7p($table, $column, $in, $column1, $data1, $colum2, $data2)
    {
        return $query = $this->db->query("select * from $table where $column in ($in) and $column1 = '$data1' and $colum2 = '$data2'")->result_array();
    }

    public function notin3p($table, $column, $in)
    {
        return $query = $this->db->query("select * from $table where $column not in ($in)")->result_array();
    }

    public function limit($start, $content_per_page, $table)
    {
        $sql = "SELECT * FROM  $table order by id desc LIMIT $start,$content_per_page";
        $result = $this->db->query($sql);
        return $result->result_array();
    }

    public function last_ai($table)
    {
        $query = $this->db->query("SHOW TABLE STATUS LIKE '$table'")->result_array();
        return $query[0]['Auto_increment'];
    }

    //SPESIFIK QUERY -->
    public function teacher_list($c_code)
    {
        $query = $this->db->query("select * from teachers where t_code in (select t_code from c_matrixs where c_code = '$c_code')")->result_array();
        return $query;
    }

    public function course_list($t_code)
    {
        $query = $this->db->query("select * from courses where c_code in (select c_code from c_matrixs where t_code = '$t_code')")->result_array();
        return $query;
    }
    
}
