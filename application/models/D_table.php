<?php
defined('BASEPATH') or exit('No direct script access allowed');

class D_table extends CI_Model
{
    public function Datatables($dt)
    {
        $columns = implode(', ', $dt['col-display']) . ', ' . $dt['id-table'];
        if($dt['table'] == 'bills'){
            $sql = "SELECT {$columns} FROM {$dt['table']} WHERE period = {$dt['param']}";            
        }else if($dt['table'] == 'teachers' || $dt['table'] == 'members'){
            $sql = "SELECT {$columns} FROM {$dt['table']} WHERE status = {$dt['param']}";            
        }else{
            $sql = "SELECT {$columns} FROM {$dt['table']}";
        }
        
        $data = $this->db->query($sql);
        $rowCount = $data->num_rows();
        $data->free_result();
        $columnd = $dt['col-display'];
        $count_c = count($columnd);
        $search = $dt['search']['value'];
        $where = '';
        if ($search != '') {
            for ($i = 0; $i < $count_c; $i++) {
                $where .= $columnd[$i] . ' LIKE "%' . $search . '%"';

                if ($i < $count_c - 1) {
                    if($dt['table'] != 'bills'){
                        $where .= ' OR ';
                    }else{
                        $where .= ' AND ';
                    }
                }
            }
        }
        for ($i = 0; $i < $count_c; $i++) {
            $searchCol = $dt['columns'][$i]['search']['value'];
            if ($searchCol != '') {
                if ($i == 3 && $dt['table'] == 'bills') {
                    $key = $this->db->query('select m_code from members where name like "%' . $searchCol . '%"')->result_array();
                    $in = '';
                    foreach ($key as $key => $value) {
                        if ($in == '') {
                            $in = '"' . $value['m_code'] . '"';
                        } else {
                            $in = $in . ',"' . $value['m_code'] . '"';
                        }
                    }
                    if ($in != '') {
                        $where = $columnd[$i] . ' IN (' . $in . ')';
                    } else {
                        $where = $columnd[$i] . ' LIKE "%' . $searchCol . '%" ';
                    }
                } else {
                    $where = $columnd[$i] . ' LIKE "%' . $searchCol . '%" ';
                }
                break;

            }
        }
        if ($where != '') {
            if($dt['table'] != 'bills' || $dt['table'] != 'teachers' || $dt['table'] != 'members'){
                $sql .= " WHERE " . $where;
            }else{
                $sql .= " AND " . $where;
            }
        }
        $sql .= " ORDER BY {$columnd[$dt['order'][0]['column']]} {$dt['order'][0]['dir']}";
        $start = $dt['start'];
        $length = $dt['length'];
        $sql .= " LIMIT {$start}, {$length}";
        $list = $this->db->query($sql);
        $option['draw'] = $dt['draw'];
        $option['recordsTotal'] = $rowCount;
        $option['recordsFiltered'] = $rowCount;
        $option['data'] = array();

        $no = 1;
        foreach ($list->result_array() as $row => $val) {
            $rows = array();

            if (
                $dt['table'] == 'users' || $dt['table'] == 'teachers' ||
                $dt['table'] == 'members' || $dt['table'] == 'courses' | $dt['table'] == 'bills'
            ) {
                $id = 0;
                $data = 1;
                foreach ($dt['col-display'] as $key => $kolom) {
                    if ($id == 0) {
                        $id = $val[$kolom];
                        $rows[] = $no;
                    } else if ($data == 2 && $dt['table'] == 'courses') {
                        $c_code = $val[$kolom];
                        $rows[] = $val[$kolom];
                    } else if ($data == 2 && $dt['table'] == 'teachers') {
                        $t_code = $val[$kolom];
                        $rows[] = $val[$kolom];
                    } else if ($data == 4 && $dt['table'] == 'bills') {
                        $rows[] = $this->main_model->gdo4p('members', 'name', 'm_code', $val[$kolom]);
                    } else {
                        $rows[] = $val[$kolom];
                    }
                    $data++;
                }

                if ($dt['table'] == 'courses') {
                    $rows[] =
                    '<a title="Update" class="btn btn-info btn-xs waves-effect"
                    onclick="edit(' . "'" . $id . "'" . ');""><i class="material-icons">mode_edit</i></a>
                    | <a title="Delete" class="btn btn-danger btn-xs waves-effect"
                    onclick="destroy(' . "'" . $id . "'" . ');"><i class="material-icons">delete</i></a>
                    | <a title="Matrix" class="btn btn-success btn-xs waves-effect"
                    onclick="matrix(' . "'" . $c_code . "'" . ');"><i class="material-icons">shuffle</i></a>';
                } else if ($dt['table'] == 'teachers') {
                    if($dt['param'] == 1){
                        $rows[] =
                        '<a title="Update" class="btn btn-info btn-xs waves-effect"
                        onclick="edit(' . "'" . $id . "'" . ');""><i class="material-icons">mode_edit</i></a>
                        | <a title="Delete" class="btn btn-danger btn-xs waves-effect"
                        onclick="destroy(' . "'" . $id . "'" . ');"><i class="material-icons">delete</i></a>
                        | <a title="Course Prices" class="btn btn-success btn-xs waves-effect"
                        onclick="price(' . "'" . $t_code . "'" . ');"><i class="material-icons">attach_money</i></a>';
                    }else{
                        $rows[] =
                        '<a title="Activate" style="width: 100%;" class="btn btn-success btn-xs waves-effect"
                        onclick="activate(' . "'" . $id . "'" . ');""><i class="material-icons">vpn_key</i> Activate</a>';
                    }
                    
                }else if ($dt['table'] == 'bills') {
                    $rows[] =
                    '<a title="Billing" class="btn btn-success btn-xs waves-effect"
                    onclick="detail(' . "'" . $id . "'" . ',' . "'" . $dt['param'] . "'" . ');""><i class="material-icons">monetization_on</i></a>
                    | <a title="Delete" class="btn btn-danger btn-xs waves-effect"
                    onclick="destroy(' . "'" . $id . "'" . ');"><i class="material-icons">delete</i></a>';
                } else if($dt['table'] == 'members'){
                    if($dt['param'] == 1){
                        $rows[] =
                        '<a title="Update" class="btn btn-info btn-xs waves-effect"
                        onclick="edit(' . "'" . $id . "'" . ');""><i class="material-icons">mode_edit</i></a>
                        | <a title="Delete" class="btn btn-danger btn-xs waves-effect"
                        onclick="destroy(' . "'" . $id . "'" . ');"><i class="material-icons">delete</i></a>';
                    }else{
                        $rows[] =
                        '<a title="Activate" style="width: 100%;" class="btn btn-success btn-xs waves-effect"
                        onclick="activate(' . "'" . $id . "'" . ');""><i class="material-icons">vpn_key</i> Activate</a>';
                    }
                }else {
                    $rows[] =
                    '<a title="Update" class="btn btn-info btn-xs waves-effect"
                    onclick="edit(' . "'" . $id . "'" . ');""><i class="material-icons">mode_edit</i></a>
                    | <a title="Delete" class="btn btn-danger btn-xs waves-effect"
                    onclick="destroy(' . "'" . $id . "'" . ');"><i class="material-icons">delete</i></a>';
                }
            }

            $no++;
            $option['data'][] = $rows;

        }
        echo json_encode($option);
    }

    public function gdo4p($table, $take, $column, $id)
    {
        $this->db->where($column, $id);
        $query = $this->db->get($table)->result_array();
        if ($query) {
            if ($query[0][$take]) {
                return $query[0][$take];
            }
        }

    }

    public function count($table, $column, $id)
    {
        $query = $this->db->query("select count($column) as total_row from $table where $column = '$id'")->result_array();
        if ($query[0]['total_row'] != '') {
            return $query[0]['total_row'];
        } else {
            return 0;
        }
    }
}
