<?php include 'layout/header.php';?>

<body class="login-page">
	<div class="login-box">
		<div class="logo">
			<a href="javascript:void(0);">Zis -
				<b>EDU</b>
			</a>
			<small>Learing Is Fun</small>
		</div>
		<div class="card">
			<div class="body">
				<form id="sign_in" method="POST">
					<div class="msg">Sign in to start your session</div>
					<div class="input-group">
						<span class="input-group-addon">
							<i class="material-icons">email</i>
						</span>
						<div class="form-line">
							<input type="text" class="form-control" id="email" placeholder="Email" required autofocus>
						</div>
					</div>
					<div class="input-group">
						<span class="input-group-addon">
							<i class="material-icons">lock</i>
						</span>
						<div class="form-line">
							<input type="password" class="form-control" id="password" placeholder="Password" required>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-8 p-t-5">

						</div>
						<div class="col-xs-4">
							<a onclick="signin()" class="btn btn-block bg-pink waves-effect" id="signin">SIGN IN</a>
						</div>
					</div>
					<div class="row m-t-15 m-b--20">
						<div class="col-xs-6">
							<!-- <a href="<?php echo base_url('signup') ?>">Register Now!</a> -->
						</div>
						<div class="col-xs-6 align-right">
							<a href="<?php echo base_url('forgot_password') ?>">Forgot Password?</a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>

	<?php include 'layout/footer.php';?>

	<script id="signinjs">
		var input = document.getElementById("email");

		input.addEventListener("keyup", function (event) {
			if (event.keyCode === 13) {
				event.preventDefault();
				document.getElementById("signin").click();
			}
		});

		var input1 = document.getElementById("password");

		input1.addEventListener("keyup", function (event) {
			if (event.keyCode === 13) {
				event.preventDefault();
				document.getElementById("signin").click();
			}
		});

		document.getElementById('signinjs').innerHTML = "";
	</script>