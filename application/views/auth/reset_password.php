<?php include 'layout/header.php';?>

<body class="fp-page">
	<div class="fp-box">
		<div class="logo">
			<a href="javascript:void(0);">Zis -
				<b>EDU</b>
			</a>
			<small>Learning Is Fun</small>
		</div>
		<div class="card">
			<div class="body">
				<div class="msg">
					Crease your new password.
				</div>
				<div class="input-group">
					<span class="input-group-addon">
						<i class="material-icons">lock</i>
					</span>
					<div class="form-line">
						<input onkeyup="check_password()" type="password" class="form-control" id="password"
							placeholder="Password" required autofocus>
					</div>
					<div style="display:none;color:red" id="passless">Min 8 character.</div>

				</div>
				<div class="input-group">
					<span class="input-group-addon">
						<i class="material-icons">lock</i>
					</span>

					<div class="form-line">
						<input onkeyup="check_password()" type="password" class="form-control" id="confirm"
							placeholder="Confirm Password" required autofocus>
					</div>
					<div style="display:none;color:red" id="conless">Min 8 character.</div>
					<div style="display:none;color:red" id="unmatch">Password not same.</div>

				</div>

				<button disabled id="save" onclick="save_password()"
					class="btn btn-block btn-lg bg-pink waves-effect">Save</button>
				<button style="display:none" id="loading" class="btn btn-block btn-lg bg-pink waves-effect">Loading
					..</button>
				<div class="row m-t-20 m-b--5 align-center">
					<a href="<?echo base_url('signin') ?>">Sign In!</a>
				</div>
			</div>
		</div>
	</div>

	<?php include 'layout/footer.php';?>

	<script id="resetpasswordjs">
		function save_password() {
			$("#save").hide();
			$("#loading").show();
			var data = {
				email: "<?php echo $email ?>",
				password: $("#password").val(),
				confirm: $("#confirm").val()
			};

			postData("<?php echo base_url() ?>auth_controller/save_new_password", data, function (err, response) {
				if (response) {
					if (response.status == "success") {
						swal("Success", "Password saved.", "success");
						setTimeout(() => {
							window.location = '<?php echo base_url() ?>signin';
						}, 1000);
					} else {
						swal("Oops..!", "Save failed. ", "error");
					}
				}
			});
		}
		document.getElementById('resetpasswordjs').innerHTML = "";
	</script>