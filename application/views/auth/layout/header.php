<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<title>Apps | Zis - EDU</title>

	<link rel="icon" href="<?php echo base_url() ?>assets/images/ziz.png" type="image/x-icon">
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url() ?>assets/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">
	<link href="<?php echo base_url() ?>assets/plugins/node-waves/waves.css" rel="stylesheet" />
	<link href="<?php echo base_url() ?>assets/plugins/animate-css/animate.css" rel="stylesheet" />
	<link href="<?php echo base_url() ?>assets/css/style.css" rel="stylesheet">
	<link href="<?php echo base_url() ?>assets/plugins/sweetalert/sweetalert.css" rel="stylesheet">
</head>
