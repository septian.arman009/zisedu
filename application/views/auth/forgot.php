<?php include 'layout/header.php';?>

<body class="fp-page">
	<div class="fp-box">
		<div class="logo">
			<a href="javascript:void(0);">Zis -
				<b>EDU</b>
			</a>
			<small>Learning Is Fun</small>
		</div>
		<div class="card">
			<div class="body">
				<div class="msg">
					Enter your email address that you used to register, We'll send you a link to reset your password.
				</div>
				<div class="input-group">
					<span class="input-group-addon">
						<i class="material-icons">email</i>
					</span>
					<div class="form-line">
						<input onkeyup="f_check()" type="email" class="form-control" id="email" placeholder="Email"
							required autofocus>
					</div>
					<div style="display:none;color:red" id="invalid_email">Please type right format of email.</div>
					<div style="display:none;color:red" id="invalid">Email not found.</div>
				</div>

				<button disabled id="send_mail" onclick="send_token()"
					class="btn btn-block btn-lg bg-pink waves-effect">Send</button>
				<button style="display:none" id="loading" class="btn btn-block btn-lg bg-pink waves-effect">Loading
					..</button>
				<div class="row m-t-20 m-b--5 align-center">
					<a href="<?php echo base_url('signin') ?>">Sign In!</a>
				</div>
			</div>
		</div>
	</div>
	<?php include 'layout/footer.php';?>