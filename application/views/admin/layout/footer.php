<script src="<?php echo base_url() ?>assets/plugins/jquery/jquery.min.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/bootstrap/js/bootstrap.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/node-waves/waves.js"></script>
<script src="<?php echo base_url() ?>assets/js/admin.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/sweetalert/sweetalert.min.js"></script>
<script src="<?php echo base_url() ?>assets/myjs/main.js"></script>
<script src="<?php echo base_url() ?>assets/myjs/load.js"></script>
<script src="<?php echo base_url() ?>assets/myjs/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url() ?>assets/myjs/jquery.maskMoney.js"></script>
<script src="<?php echo base_url() ?>assets/myjs/bootstrap3-typeahead.min.js"></script>
<script src="<?php echo base_url() ?>assets/HC/code/highcharts.js"></script>

<script src="<?php echo base_url() ?>assets/plugins/momentjs/moment.js"></script>
<script
	src="<?php echo base_url() ?>assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js">
</script>


<script id="footerjs">
	$(document).ready(function () {
		start_page();
	});
	function removeDropdown() {
		$(".user-helper-dropdown").removeClass("open");
	}
	document.getElementById('footerjs').innerHTML = "";
</script>

<style>
	.form-control:focus {
		border-bottom: 2px solid <?php echo $table_color ?>;
	}

	.input-group .form-control:focus {
		border-bottom: 2px solid <?php echo $table_color ?>;
	}

	th#th {
		background-color: <?php echo $table_color ?>;
		color: white;
	}

	.no-sort::after {
		display: none !important;
	}

	.no-sort {
		pointer-events: none !important;
		cursor: default !important;
	}

	.sidebar .user-info {
		background-color: <?php echo $table_color ?>;
	}

	.active_c {
		font-weight: bold;
		font-style: italic;
	}

	.pointer {
		cursor: pointer;
	}
</style>

</body>


</html>