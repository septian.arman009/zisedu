<section>
	<!-- Left Sidebar -->
	<aside id="leftsidebar" class="sidebar">
		<!-- User Info -->
		<div class="user-info">
			<div class="image">
				<div id="ajax-loading">
					<div class="loader">
						<div class="preloader pl-size-sm">
							<div class="spinner-layer pl-red">
								<div class="circle-clipper left">
									<div class="circle"></div>
								</div>
								<div class="circle-clipper right">
									<div class="circle"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<img src="<?php echo base_url() ?>assets/images/ziz.png" width="48" height="48" id="side_img" />
			</div>
			<div class="info-container">
				<div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<?php echo $_SESSION['bim_in']['name'] ?>
				</div>
				<div class="email">
					<?php echo $_SESSION['bim_in']['email'] ?>
				</div>
				<div class="btn-group user-helper-dropdown">
					<i class="material-icons" data-toggle="dropdown" aria-haspopup="true"
						aria-expanded="true">keyboard_arrow_down</i>
					<ul class="dropdown-menu pull-right">
						<li>
							<a href="javascript:void(0);"
								onclick="main_menu('#','master_controller/form/user/<?php echo $_SESSION['bim_in']['id'] ?>')">
								<i class="material-icons">person</i>Profile</a>
						</li>
						<li>
							<a onclick="logout()" href="javascript:void(0);">
								<i class="material-icons">input</i>Sign Out</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- #User Info -->
		<!-- Menu -->
		<div class="menu">
			<ul class="list">
				<li class="header">MAIN NAVIGATION</li>
				<li id="home">
					<a onclick="main_menu('#home','main_controller/home')">
						<i class="material-icons">home</i>
						<span>Home</span>
					</a>
				</li>

				<li id="registration">
					<a onclick="main_menu('#registration','reg_controller/registration/<?php echo $date_now[0] ?>')">
						<i class="material-icons">assignment</i>
						<span>Member Course Reg</span>
					</a>
				</li>

				<li id="master_data">
					<a class="menu-toggle" id="master_data_toggle">
						<i class="material-icons">list</i>
						<span>Master</span>
					</a>
					<ul class="ml-menu">
						<li id="user">
							<a onclick="sub_menu('#master_data','#user','master_controller/user')">Users</a>
						</li>
						<li id="course">
							<a onclick="sub_menu('#master_data','#course','master_controller/course')">Courses</a>
						</li>
						<li id="teacher">
							<a onclick="sub_menu('#master_data','#teacher','master_controller/teacher/1')">Teachers</a>
						</li>
						<li id="member">
							<a onclick="sub_menu('#master_data','#member','master_controller/member/1')">Members</a>
						</li>
					</ul>
				</li>

				<li id="billing">
					<a onclick="main_menu('#billing','payment_controller/billing/<?php echo $date_now[0] ?>')">
						<i class="material-icons">attach_money</i>
						<span>Billing</span>
					</a>
				</li>

				<li id="report">
					<a class="menu-toggle" id="report_toggle">
						<i class="material-icons">book</i>
						<span>Report</span>
					</a>
					<ul class="ml-menu">
						<li id="monthly">
							<a onclick="sub_menu('#report','#monthly','report_controller/monthly/<?php echo $date_now[1].'/'.$date_now[0] ?>')">Monthly Report</a>
						</li>
					</ul>
				</li>

			</ul>
		</div>
		<!-- #Menu -->
		<!-- Footer -->
		<div class="legal">
			<div class="copyright">
				&copy; 2019
				<a href="javascript:void(0);">Zis - Education Apps</a>.
			</div>
			<div class="version">
				<b>Version: </b> 1.0.0
			</div>
		</div>
		<!-- #Footer -->
	</aside>
	<!-- #END# Left Sidebar -->
	<!-- Right Sidebar -->
	<aside id="rightsidebar" class="right-sidebar">
		<ul class="nav nav-tabs tab-nav-right" role="tablist">
			<li role="presentation" class="active">
				<a href="#skins" data-toggle="tab">SKINS</a>
			</li>
			<li role="presentation">
				<a href="#settings" data-toggle="tab">SETTINGS</a>
			</li>
		</ul>
		<div class="tab-content">
			<div role="tabpanel" class="tab-pane fade in active in active" id="skins">
				<ul class="demo-choose-skin">
					<?php if ($theme == 'theme-red') {?>
					<li onclick="setTheme('theme-red')" class="active" data-theme="red">
						<?php } else {?>
					<li onclick="setTheme('theme-red')" data-theme="red">
						<?php }?>
						<div class="red"></div>
						<span>Red</span>
					</li>

					<?php if ($theme == 'theme-pink') {?>
					<li onclick="setTheme('theme-pink')" class="active" data-theme="pink">
						<?php } else {?>
					<li onclick="setTheme('theme-pink')" data-theme="pink">
						<?php }?>
						<div class="pink"></div>
						<span>Pink</span>
					</li>

					<?php if ($theme == 'theme-purple') {?>
					<li onclick="setTheme('theme-purple')" class="active" data-theme="purple">
						<?php } else {?>
					<li onclick="setTheme('theme-purple')" data-theme="purple">
						<?php }?>
						<div class="purple"></div>
						<span>Purple</span>
					</li>

					<?php if ($theme == 'theme-deep-purple') {?>
					<li onclick="setTheme('theme-deep-purple')" class="active" data-theme="deep-purple">
						<?php } else {?>
					<li onclick="setTheme('theme-deep-purple')" data-theme="deep-purple">
						<?php }?>
						<div class="deep-purple"></div>
						<span>Deep Purple</span>
					</li>

					<?php if ($theme == 'theme-indigo') {?>
					<li onclick="setTheme('theme-indigo')" class="active" data-theme="indigo">
						<?php } else {?>
					<li onclick="setTheme('theme-indigo')" data-theme="indigo">
						<?php }?>
						<div class="indigo"></div>
						<span>Indigo</span>
					</li>

					<?php if ($theme == 'theme-blue') {?>
					<li onclick="setTheme('theme-blue')" class="active" data-theme="blue">
						<?php } else {?>
					<li onclick="setTheme('theme-blue')" data-theme="blue">
						<?php }?>
						<div class="blue"></div>
						<span>Blue</span>
					</li>

					<?php if ($theme == 'theme-light-blue') {?>
					<li onclick="setTheme('theme-blue')" class="active" data-theme="light-blue">
						<?php } else {?>
					<li onclick="setTheme('theme-light-blue')" data-theme="light-blue">
						<?php }?>
						<div class="light-blue"></div>
						<span>Light Blue</span>
					</li>

					<?php if ($theme == 'theme-cyan') {?>
					<li onclick="setTheme('theme-blue')" class="active" data-theme="cyan">
						<?php } else {?>
					<li onclick="setTheme('theme-cyan')" data-theme="cyan">
						<?php }?>
						<div class="cyan"></div>
						<span>Cyan</span>
					</li>

					<?php if ($theme == 'theme-teal') {?>
					<li onclick="setTheme('theme-teal')" class="active" data-theme="teal">
						<?php } else {?>
					<li onclick="setTheme('theme-teal')" data-theme="teal">
						<?php }?>
						<div class="teal"></div>
						<span>Teal</span>
					</li>

					<?php if ($theme == 'theme-green') {?>
					<li onclick="setTheme('theme-green')" class="active" data-theme="green">
						<?php } else {?>
					<li onclick="setTheme('theme-green')" data-theme="green">
						<?php }?>
						<div class="green"></div>
						<span>Green</span>
					</li>

					<?php if ($theme == 'theme-light-green') {?>
					<li onclick="setTheme('theme-light-green')" class="active" data-theme="light-green">
						<?php } else {?>
					<li onclick="setTheme('theme-light-green')" data-theme="light-green">
						<?php }?>
						<div class="light-green"></div>
						<span>Light Green</span>
					</li>

					<?php if ($theme == 'theme-lime') {?>
					<li onclick="setTheme('theme-lime')" class="active" data-theme="lime">
						<?php } else {?>
					<li onclick="setTheme('theme-lime')" data-theme="lime">
						<?php }?>
						<div class="lime"></div>
						<span>Lime</span>
					</li>

					<?php if ($theme == 'theme-yellow') {?>
					<li onclick="setTheme('theme-yellow')" class="active" data-theme="yellow">
						<?php } else {?>
					<li onclick="setTheme('theme-yellow')" data-theme="yellow">
						<?php }?>
						<div class="yellow"></div>
						<span>Yellow</span>
					</li>

					<?php if ($theme == 'theme-amber') {?>
					<li onclick="setTheme('theme-amber')" class="active" data-theme="amber">
						<?php } else {?>
					<li onclick="setTheme('theme-amber')" data-theme="amber">
						<?php }?>
						<div class="amber"></div>
						<span>Amber</span>
					</li>

					<?php if ($theme == 'theme-orange') {?>
					<li onclick="setTheme('theme-orange')" class="active" data-theme="orange">
						<?php } else {?>
					<li onclick="setTheme('theme-orange')" data-theme="orange">
						<?php }?>
						<div class="orange"></div>
						<span>Orange</span>
					</li>

					<?php if ($theme == 'theme-deep-orange') {?>
					<li onclick="setTheme('theme-deep-orange')" class="active" data-theme="deep-orange">
						<?php } else {?>
					<li onclick="setTheme('theme-deep-orange')" data-theme="deep-orange">
						<?php }?>
						<div class="deep-orange"></div>
						<span>Deep Orange</span>
					</li>

					<?php if ($theme == 'theme-brown') {?>
					<li onclick="setTheme('theme-brown')" class="active" data-theme="brown">
						<?php } else {?>
					<li onclick="setTheme('theme-brown')" data-theme="brown">
						<?php }?>
						<div class="brown"></div>
						<span>Brown</span>
					</li>

					<?php if ($theme == 'theme-grey') {?>
					<li onclick="setTheme('theme-grey')" class="active" data-theme="grey">
						<?php } else {?>
					<li onclick="setTheme('theme-grey')" data-theme="grey">
						<?php }?>
						<div class="grey"></div>
						<span>Grey</span>
					</li>

					<?php if ($theme == 'theme-blue-grey') {?>
					<li onclick="setTheme('theme-blue-grey')" class="active" data-theme="blue-grey">
						<?php } else {?>
					<li onclick="setTheme('theme-blue-grey')" data-theme="blue-grey">
						<?php }?>
						<div class="blue-grey"></div>
						<span>Blue Grey</span>
					</li>

					<?php if ($theme == 'theme-black') {?>
					<li onclick="setTheme('theme-black')" class="active" data-theme="black">
						<?php } else {?>
					<li onclick="setTheme('theme-black')" data-theme="black">
						<?php }?>
						<div class="black"></div>
						<span>Black</span>
					</li>
				</ul>
			</div>
			<div role="tabpanel" class="tab-pane fade" id="settings">
				<div class="demo-settings">
					<p>System Setting</p>
					<ul class="setting-list">
						<!-- <li>
							<span>Bill by Email</span>
							<div class="switch">
								<label>
									<?php if($setting[0]['email'] == 'on'){ ?>
										<input id="checkbox_email" checked onclick="turn_email_sender()" type="checkbox">
									<?php }else{ ?>
										<input id="checkbox_email" onclick="turn_email_sender()" type="checkbox">
									<?php } ?>
									<span class="lever"></span>
								</label>
							</div>
						</li> -->

						<li>
							<span>Mail System : </span>
							<div class="row">

								<div class="col-md-12">
									<input onkeyup="check_sys_mail()" id="send_mail" type="email" class="form-control"
										value="<?php echo $setting[0]['send_mail'] ?>">
								</div>

							</div>
							<br>
							<span>Mail Password : </span>
							<div class="row">

								<div class="col-md-12">
									<input onkeyup="check_sys_mail()" id="send_pass" type="password"
										class="form-control" value="<?php echo $setting[0]['send_pass'] ?>">
								</div>

							</div>
							<br>
							<button id="sys_btn" onclick="update_sys_mail()" style="border-radius:100px;width:100%;"
								class="btn btn-primary waves-effect">Update</button>

						</li>

						<!-- <li>
							<span>Max Payment Date : </span>
							<div class="row">
								<div class="col-md-8">
									<input id="max_payment_date" type="number" class="form-control"
										value="<?php echo $setting[0]['max_payment_date'] ?>">
								</div>
								<div class="col-md-2">
									<button style="border-radius:100px;" class="btn btn-primary waves-effect"
										onclick="max_payment_date()">Update</button>
								</div>
							</div>
						</li>

						<li>
							<span>Max Member perTeacher : </span>
							<div class="row">
								<div class="col-md-8">
									<input id="max_member" type="number" class="form-control"
										value="<?php echo $setting[0]['max_member'] ?>">
								</div>
								<div class="col-md-2">
									<button style="border-radius:100px;" class="btn btn-primary waves-effect"
										onclick="max_member()">Update</button>
								</div>
							</div>
						</li> -->
						
					</ul>
				</div>
			</div>
		</div>
	</aside>
	<!-- #END# Right Sidebar -->
</section>

<script>
	function turn_email_sender() {

		if (_("checkbox_email").checked) {
			var checkbox_email = 'on';
		} else {
			var checkbox_email = 'off';
		}

		var data = {
			checkbox_email: checkbox_email
		}

		postData('main_controller/turn_email_sender', data, function (err, response) {
			if (response) {
				var status = response.status;
				if (status == 'success') {
					if(checkbox_email == 'on'){
						swal("Success", "Bill by email ON.", "success");
					}else{
						swal("Success", "Bill by email OFF.", "success");
					}
				} else {
					swal("Oops..!", "Update failed.", "error");
				}
			} else {
				console.log('ini error : ', err);
			}
		});
	}

	function max_member() {
		var data = {
			max_member: $("#max_member").val()
		}
		postData('main_controller/max_member', data, function (err, response) {
			if (response) {
				var status = response.status;
				if (status == 'success') {
					swal("Success", "Max member perteacher updated.", "success");
				} else {
					swal("Oops..!", "Update failed.", "error");
				}
			} else {
				console.log('ini error : ', err);
			}
		});
	}

	function max_payment_date() {
		var data = {
			max_payment_date: $("#max_payment_date").val()
		}
		postData('main_controller/max_payment_date', data, function (err, response) {
			if (response) {
				var status = response.status;
				if (status == 'success') {
					swal("Success", "Max payment date updated.", "success");
				} else {
					swal("Oops..!", "Update failed.", "error");
				}
			} else {
				console.log('ini error : ', err);
			}
		});
	}

	function check_sys_mail() {
		var email = $("#send_mail").val();
		var password = $("#send_pass").val();

		if (email != '' && password != '') {
			$('#sys_btn').removeAttr('disabled');
		} else {
			$('#sys_btn').attr('disabled', 'disabled');
		}
	}

	function update_sys_mail() {
		check_sys_mail();
		var data = {
			email: $("#send_mail").val(),
			password: $("#send_pass").val()
		}
		postData('main_controller/update_sys_mail', data, function (err, response) {
			if (response) {
				var status = response.status;
				if (status == 'success') {
					swal("Success", "System mail updated.", "success");
				} else {
					swal("Oops..!", "Update failed.", "error");
				}
			} else {
				console.log('ini error : ', err);
			}
		});
	}
</script>