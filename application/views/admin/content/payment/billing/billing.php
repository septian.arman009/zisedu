<div class="container-fluid">
	<div class="block-header">
		<h2>BILLINGS</h2>
	</div>
	<!-- Basic Table -->
	<div class="row clearfix">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="card">
                <div class="body table-responsive">
				<div class="row">
					<div class="col-md-4">
					<select onchange="filter($(this).val())" id="year" class="form-control">
							<?php for ($i = 2019; $i <= 2099; $i++) {?>
								<?php if ($i == $year) {?>
									<option selected value="<?php echo $i ?>">
										<?php echo $i ?>
									</option>
								<?php } else {?>
									<option value="<?php echo $i ?>">
										<?php echo $i ?>
									</option>
								<?php }?>
							<?php }?>
						</select>
					</div>
					<div class="col-md-8 align-right">
						<a onclick="loadView('reg_controller/renew_form/<?php echo $year ?>','.content')" class="btn btn-primary waves-effect align-right"> Renew Subscription </a>
					</div>
				</div>
				<table id="billing-table" class="table stripe hover">
					<thead>
						<tr>
							<th id="th" width="10%">No</th>
							<th id="th">Billing Code</th>
							<th id="th">Member Code</th>
                            <th id="th">Name</th>
                            <th id="th">Period</th>
							<th id="th" class="no-sort" width="10%">Action</th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<th class="footer">no</th>
							<th class="footer">Billing COde</th>
							<th class="footer">Member Code</th>
							<th class="footer">Name</th>
							<th class="footer">Period</th>
						</tr>
					</tfoot>
				</table>
                </div>
			</div>
		</div>
	</div>

	<script id="billingjs">
		$(document).ready(function () {
			$('#billing-table tfoot th').each(function () {
				var title = $(this).text();
				var inp = '<input type="text" class="form-control footer-s" id="' + title + '" placeholder="' + title + '" />';
				$(this).html(inp);
			});

			var table = $('#billing-table').DataTable({
				"processing": true,
				"serverSide": true,
				"ajax": {
					"url": 'payment_controller/billing_table/<?php echo $year ?>',
					"type": "POST"
				}
			});

			table.columns().every(function () {
				var that = this;
				$('input', this.footer()).on('keyup change', function () {
					if (that.search() !== this.value) {
						that.search(this.value).draw();
					}
				});
			});
			$("#no").hide();
		});

		function destroy(id) {
			swal({
					title: "Are you sure ?",
					text: "Data will be deleted permanently.",
					type: "warning",
					showCancelButton: true,
					confirmButtonClass: "btn-danger",
					confirmButtonText: "Yes, Delete Now",
					closeOnConfirm: false
				},
				function () {
					var data = {
						id: id
					}
					postData('main_controller/destroy/bills/id/', data, function (err, response) {
						if (response) {
							console.log('berhasil : ', response);
							if (response.status == 'success') {
								loadView('payment_controller/billing/<?php echo $year ?>','.content');
    							swal("Success", "Billing has been deleted.", "success");
							}else{
								swal("Oops", "Delete failed.", "error");

							}
						} else {
							console.log('ini error : ', err);
						}
					});
				}
			);
		}

        function detail(id, year)
        {
            loadView('payment_controller/billing_detail/'+id+'/'+year,'.content');
		}

		function filter(year)
		{
			loadView('payment_controller/billing/'+year,'.content');
		}
		
		document.getElementById('billingjs').innerHTML = "";
		
	</script>

	<style>
		#billing-table_filter{
			display:none;
		}
	</style>
