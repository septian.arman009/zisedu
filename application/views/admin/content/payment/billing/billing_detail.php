<div class="container-fluid">
	<div class="block-header">
		<h2>BILLING DETAIL</h2>
	</div>
	<!-- Basic Table -->
	<div class="row clearfix">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="card">
				<div class="header">
					<h2>
						<a class="btn btn-primary waves-effect" style="cursor:pointer"
							onclick="loadView('payment_controller/billing/<?php echo $year ?>','.content')">
							<i class="material-icons">keyboard_backspace</i>
						</a>
						Billing For Member <?php echo $member[0]['name'] ?> ( <?php echo $member[0]['m_code'] ?> )
					</h2>
				</div>
				<div class="body">
					<div class="row clearfix">

						<?php 
                            foreach ($payment as $key => $value) { 
                                if($value['status'] == 'unbilled'){ ?>

						<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
							<div class="info-box bg-silver hover-expand-effect">
								<div class="icon">
									<i class="material-icons">check</i>
								</div>
								<div class="content">
									<div class="text">
										<i class="material-icons">do_not_disturb</i>
									</div>
									<div class="number">
										<?php echo $value['month'] ?>
									</div>
								</div>
							</div>

							<button disabled="" style="width:100%;margin-top:-18%" class="btn btn-silver">
								NO BILL
							</button>
						</div>

						<?php } else if($value['status'] == 'billed'){ ?>
						<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
							<div class="info-box bg-red hover-expand-effect">
								<div class="icon">
									<i class="material-icons">close</i>
								</div>
								<div class="content">
									<div class="text">
										BILLED
									</div>
									<div class="number">
                                        <?php echo $value['month'] ?>
                                    </div>
								</div>
							</div>

							<button onclick="pay('<?php echo $id ?>', '<?php echo $value['month'] ?>')" style="width:100%;margin-top:-18%" class="btn btn-warning waves-effect">
                                PAY
                            </button>
						</div>

						<?php } else if($value['status'] == 'payed') {?>

						<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
							<div class="info-box bg-green hover-expand-effect">
								<div class="icon">
									<i class="material-icons">check_box</i>
								</div>
								<div class="content">
									<div class="text">
										PAYED
									</div>

									<div class="number">
                                        <?php echo $value['month'] ?>
								    </div>
								</div>
							</div>

							<button onclick="pay('<?php echo $id ?>', '<?php echo $value['month'] ?>')" style="width:100%;margin-top:-18%" class="btn btn-success waves-effect">
                                PAYED
                            </button>
						</div>
						<?php } }?>

					</div>
				</div>
			</div>
		</div>

		<script>
		function pay(id, month)
        {
            loadView('payment_controller/pay_form/'+id+'/'+month+'/<?php echo $year ?>', '.content');
		}
		</script>