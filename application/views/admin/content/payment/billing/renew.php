<div class="container-fluid">
	<div class="block-header">
		<h2>RENEW SUBSCRIPTION</h2>
	</div>
	<!-- Basic Table -->
	<div class="row clearfix">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="card">
				<div class="header">
					<h2>
                        <a class="btn btn-primary waves-effect" style="cursor:pointer"
							onclick="loadView('payment_controller/billing/<?php echo $year ?>','.content')">
							<i class="material-icons">keyboard_backspace</i>
						</a> 
                        Renew Subscription Form
					</h2>
				</div>
				<div class="body">

					<div class="row clearfix">
						<div class="col-sm-12">
							<div class="form-group">
								<label>Member Name</label>
								<div class="form-line">
									<input id="name" onkeyup="form_check($(this).val())" type="text"
										class="typehead form-control" />
								</div>
							</div>
							<div class="form-group">
								<label>Start From</label>
                                <div class="form-line">
                                    <select id="month" class="form-control">
                                        <?php
                                        foreach ($month as $key => $value) { 
                                            if($value == $m){ ?>
                                                <option selected value="<?php echo $value ?>"><?php echo $value ?></option>
                                        <?php }else{?>
                                                <option value="<?php echo $value ?>"><?php echo $value ?></option>
                                        <?php } } ?>
                                    </select>
                                </div>
							</div>
							<div class="form-group">
								<label>Year</label>
                                <div class="form-line">
                                    <select id="year" class="form-control">
                                        <?php for ($i = 2019; $i <= 2099; $i++) {?>
                                            <?php if ($i == $year) {?>
                                                <option selected value="<?php echo $i ?>">
                                                    <?php echo $i ?>
                                                </option>
                                            <?php } else {?>
                                                <option value="<?php echo $i ?>">
                                                    <?php echo $i ?>
                                                </option>
                                            <?php }?>
                                        <?php }?>
                                    </select>
                                </div>
							</div>
						</div>

						<div class="col-md-2">
							<div class="form-group">
								<div class="form-line">
									<button disabled id="save" onclick="renew_billing()"
										class="btn btn-block bg-pink waves-effect">Save</button>
									<button style="display:none" id="loading"
										class="btn btn-block bg-pink waves-effect">Loading ..</button>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>

<style>
	#course_name {
		font-weight: bold;
	}

	.red {
		color: red;
	}

	td {
		padding: 10px;
	}
</style>

<script id="renewjs">
	$(document).ready(function () {
		$('#name').typeahead({
			source: function (query, process) {
				return $.get('reg_controller/search_member/', {
					query: query
				}, function (data) {
					data = $.parseJSON(data);
					return process(data);
				});
			}
		});
	});

    function form_check() {
        var name = $("#name").val();
        if(name != ''){
            $("#save").removeAttr("disabled");
        }else{
            $("#save").attr("disabled", "disabled");

        }
    }

	function renew_billing(m_code) {
		$("#save").hide();
		$("#loading").show();
		var data = {
			name: $("#name").val(),
            month: $("#month").val(),
            year: $("#year").val()
		}

		postData('reg_controller/renew_billing', data, function (err, response) {
			if (response) {
				var status = response.status;
				if (status == 'success') {
					$("#save").show();
					$("#loading").hide();
					$("#name").val('');
					swal("Success", "Billing has been created.", "success");
				} else if (status == 'exist') {
					$("#save").show();
					$("#loading").hide();
					swal("Warning", "Billilng already exist.", "warning");
				} else {
					$("#save").show();
					$("#loading").hide();
					swal("Oops..!", "Billing create failed.", "error");
				}
			} else {
				console.log('ini error : ', err);
			}
		});
	}

	document.getElementById('renewjs').innerHTML = "";
</script>