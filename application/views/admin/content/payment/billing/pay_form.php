<div class="container-fluid">
	<div class="block-header">
		<h2>PAYMENT</h2>
	</div>
	<!-- Basic Table -->
	<div class="row clearfix">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="card">
				<div class="header">
					<h2>
						<a class="btn btn-primary waves-effect" style="cursor:pointer"
							onclick="loadView('payment_controller/billing_detail/<?php echo $id.'/'.$year ?>','.content')">
							<i class="material-icons">keyboard_backspace</i>
						</a>
						Payment Form for <?php echo $month.' '.$year ?>
					</h2>
				</div>
				<div class="body">
					<div class="row clearfix">
						<div class="col-sm-12">
							<div class="form-group">
								<label>Billing Amount</label>
								<div class="form-line">
									<input disabled type="text" class="form-control" id="amount"
										value="<?php echo torp($amount) ?>" />
								</div>
							</div>

							<div class="form-group">
								<label>Member Name</label>
								<div class="form-line">
									<input disabled type="text" class="form-control" id="m_name"
										value="<?php echo $m_name ?>" />
								</div>
							</div>

							<div class="form-group">
								<label>Proof Of Payment</label>
								<div class="form-line">
									<?php if ($img != '') {?>
									<a href="<?php echo base_url() ?>assets/bill_img/<?php echo $img ?>"
										target="_blank">
										<img width="200" height="200"
											src="<?php echo base_url() ?>assets/bill_img/<?php echo $img ?>">
									</a>
									<?php }?>
									<input type="file" name="file" name="file" class="form-control" />
								</div>
							</div>

							<div class="form-group">
								<label>Payment Date</label>
								<div class="form-line">
									<input type="text" class="pointer datepicker form-control" id="payment_date"
										value="<?php if($payment_date != ''){echo to_date($payment_date);}?>" />
								</div>
							</div>
						</div>

						<div class="col-md-2">
							<div class="form-group">
								<div class="form-line">
									<button id="save" onclick="action()"
										class="btn btn-block bg-pink waves-effect"><?php if($payment_date == ''){echo 'Save';}else{echo 'Update';} ?></button>
									<button style="display:none" id="loading"
										class="btn btn-block bg-pink waves-effect">Loading
										..</button>
								</div>
							</div>
						</div>
						<?php if(role(['admin', 'owner'], false)){ ?>
							<div class="col-md-2">
								<div class="form-group">
									<div class="form-line">
										<button id="reset" onclick="reset()"
											class="btn btn-block bg-red waves-effect">Reset</button>
									</div>
								</div>
							</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	var id = '<?php echo $id ?>';

	$(document).ready(function () {
		$('.datepicker').bootstrapMaterialDatePicker({
			format: 'DD MMMM YYYY',
			clearButton: false,
			weekStart: 1,
			time: false
		});
	});

	function reset() {
		swal({
				title: "Are you sure ?",
				text: "Payment will be permanently deleted.",
				type: "warning",
				showCancelButton: true,
				confirmButtonClass: "btn-danger",
				confirmButtonText: "Yes, Reset Now",
				closeOnConfirm: false
			},
			function () {
				var data = {
					id: id,
					month: '<?php echo $month ?>',
				}

				postData('payment_controller/reset', data, function (err, response) {
					if (response) {
						var status = response.status;
						if (status == 'success') {
							swal("Success", "Payment has been reset.", "success");
							loadView('payment_controller/pay_form/' + id + '/<?php echo $month.'/'.$year ?>',
								'.content');
						} else {
							swal("Oops..!", "Reset failed.", "error");
						}
					} else {
						console.log('ini error : ', err);
					}
				});
			}
		);
	}

	function action() {
		$("#save").hide();
		$("#loading").show();
		var inputFile = $('input[name=file]');
		var fileToUpload = inputFile[0].files[0];
		var payment_date = $("#payment_date").val();
		var month = '<?php echo $month ?>';

		if (payment_date != '') {
			var formData = new FormData();
			formData.append("id", id);
			formData.append("index", '<?php echo $index ?>');
			formData.append("amount", $("#amount").val());
			formData.append("payment_date", payment_date);
			formData.append("file", fileToUpload);

			$.ajax({
				url: 'payment_controller/pay',
				type: 'post',
				data: formData,
				processData: false,
				contentType: false,
				success: function () {
					if (month == 'December') {
						loadView('payment_controller/billing_detail/<?php echo $id.'/'.$year ?>',
						'.content');
						swal("Success", 'Payment has been saved.', "success");
						setTimeout(() => {
							renew();
						}, 500);
					} else {
						$("#save").show();
						$("#loading").hide();
						loadView('payment_controller/billing_detail/<?php echo $id.'/'.$year ?>',
						'.content');
						swal("Success", 'Payment has been saved.', "success");
					}
				}
			});
		} else {
			$("#save").show();
			$("#loading").hide();
			swal("Oops..!", 'Please complete the form to continue.', "error");
		}
	}

	function renew(id) {
		swal({
				title: "Renew subscription ?",
				text: "New billing will be create for 12 month.",
				type: "warning",
				showCancelButton: true,
				confirmButtonClass: "btn-danger",
				confirmButtonText: "Yes, Renew Now",
				closeOnConfirm: false
			},
			function () {
				var data = {}
				postData('reg_controller/renew/<?php echo $m_code.'/'.($year+1) ?>', data, function (err, response) {
					if (response) {
						console.log('berhasil : ', response);
						if (response.status == 'success') {
							loadView('payment_controller/billing/<?php echo $year ?>', '.content');
							swal("Success", "Billing has been created.", "success");
						} else {
							loadView('payment_controller/billing/<?php echo $year ?>', '.content');
							swal("Oops..!", "Billing exist.", "error");
						}
					}
				});
			}
		);
	}
</script>