<?php

$this->fpdf->FPDF_17('P', 'cm', 'A4');
$this->fpdf->AliasNbPages();
$this->fpdf->AddPage();

$this->fpdf->Image('assets/images/ziz.png', 1, 1, 2.5, 0, '', base_url('admin'));

$this->fpdf->SetFont('Times', 'B', 10);
$this->fpdf->Cell(0, 0, '', 0, 0, 'C');

$this->fpdf->SetFont('Times', 'B', 10);
$this->fpdf->Ln(0.5);
$this->fpdf->Cell(0, 0, '', 0, 0, 'C');

$this->fpdf->SetFont('Times', 'B', 24);
$this->fpdf->Ln(0.5);
$this->fpdf->Cell(0, 0, 'Zis - Education Course', 0, 0, 'C');

$this->fpdf->SetFont('Times', '', 12);
$this->fpdf->Ln(0.6);
$this->fpdf->Cell(0, 0, 'Jl. Kh. Agus Salim Kp. Bulkan Slamet RT06/RW08 No.9', 0, 0, 'C');

$this->fpdf->SetFont('Times', '', 12);
$this->fpdf->Ln(0.6);
$this->fpdf->Cell(0, 0, 'Bekasi Jaya, Bekasi Timur 17120', 0, 0, 'C');

$this->fpdf->Ln(0.5);
$this->fpdf->Line(1, 3.6, 20, 3.6);
$this->fpdf->Ln(0.5);
$this->fpdf->Line(1, 3.7, 20, 3.7);

if ($transaction) {
    $this->fpdf->SetFont('Times', 'B', 12);
    $this->fpdf->Ln(0.6);
    $this->fpdf->Cell(0, 0, 'Report for ' . $month . ' ' . $year, 0, 0, 'C');

    $this->fpdf->SetFont('Times', 'B', 12);
    $this->fpdf->ln(1);
    $this->fpdf->Cell(2, 1, 'No', 1, 0, 'C');
    $this->fpdf->Cell(5, 1, 'Transaction Code', 1, 0, 'C');
    $this->fpdf->Cell(4.5, 1, 'Member', 1, 0, 'C');
    $this->fpdf->Cell(4, 1, 'Num of Courses', 1, 0, 'C');
    $this->fpdf->Cell(3.5, 1, 'Amount', 1, 0, 'C');

    $this->fpdf->Ln();

    $no = 1;
    foreach ($transaction as $key => $value) {

        $this->fpdf->SetFont('Times', '', 11);
        $this->fpdf->Cell(2, 0.5, $no++, 1, 0, 'C');
        $this->fpdf->Cell(5, 0.5, $value['tc_code'], 1, 0, 'C');
        $this->fpdf->Cell(4.5, 0.5, $value['m_name'], 1, 0, 'C');
        $this->fpdf->Cell(4, 0.5, count($value['course']) . " Course", 1, 0, 'C');
        $this->fpdf->Cell(3.5, 0.5, torp($value['amount']), 1, 0, 'L');

        $this->fpdf->Ln();

    }

    $this->fpdf->SetFont('Times', 'B', 11);
        $this->fpdf->Cell(2, 0.5, "",0, 0, 'C');
        $this->fpdf->Cell(5, 0.5, "", 0, 0, 'C');
        $this->fpdf->Cell(4.5, 0.5, "", 0, 0, 'C');
        $this->fpdf->Cell(4, 0.5, "Total Amount", 1, 0, 'C');
        $this->fpdf->Cell(3.5, 0.5, torp($total), 1, 0, 'L');

        $this->fpdf->Ln();
} else {
    $this->fpdf->SetFont('Times', 'B', 12);
    $this->fpdf->Ln(0.6);
    $this->fpdf->Cell(0, 0, "No data to show.", 0, 0, 'C');
}

$this->fpdf->Ln();

$this->fpdf->Output();
