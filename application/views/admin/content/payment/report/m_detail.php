    <?php $no = 1; foreach ($transaction as $key => $value) { ?>
    <tr>
    	<td><?php echo $no++ ?></td>
    	<td><?php echo $value['tc_code'] ?></td>
    	<td><?php echo $value['m_name'] ?></td>
    	<td>
    		<?php foreach ($value['course'] as $key => $vc) { ?>
    		<table class="table-borderless">
    			<tbody>
    				<tr>
    					<td>Name</td>
    					<td>:</td>
    					<td><?php echo $vc['name']." ({$vc['c_code']})" ?></td>
    				</tr>
    				<tr>
    					<td>Teacher</td>
    					<td>:</td>
    					<td><?php echo $this->main_model->gdo4p('teachers', 'name', 't_code', $vc['t_code']) ?></td>
    				</tr>
    				<tr>
    					<td>Price</td>
    					<td>:</td>
    					<td><?php echo torp($vc['price']) ?></td>
    				</tr>
    			</tbody>
    		</table>
    		<br>
    		<?php } ?>
    	</td>
    	<td><?php echo torp($value['amount']) ?></td>
    </tr>
    <?php } ?>

    <tr>
    	<td></td>
    	<td></td>
    	<td></td>
    	<td> <div class="align-right" style="font-weight: bold">TOTAL AMOUNT</div></td>
    	<td style="font-weight: bold"><?php echo torp($total) ?></td>
    </tr>

    <style>
    	.table-borderless>tbody>tr>td,
    	.table-borderless>tbody>tr>th,
    	.table-borderless>tfoot>tr>td,
    	.table-borderless>tfoot>tr>th,
    	.table-borderless>thead>tr>td,
    	.table-borderless>thead>tr>th {
    		border: none;
    	}
    </style>