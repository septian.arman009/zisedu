<div class="container-fluid">
	<div class="block-header">
		<h2>MONTHLY REPORT</h2>
	</div>

	<!-- Basic Table -->
	<div class="row clearfix">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="card">
				<div class="body">

					<div class="row">
						<div class="col-md-4">
							<select onchange="filter($(this).val())" id="year" class="form-control">
								<?php for ($i = 2019; $i <= 2099; $i++) {?>
								<?php if ($i == $year) {?>
								<option selected value="<?php echo $i ?>">
									<?php echo $i ?>
								</option>
								<?php } else {?>
								<option value="<?php echo $i ?>">
									<?php echo $i ?>
								</option>
								<?php }?>
								<?php }?>
							</select>
						</div>
					</div>

					<div class="row">
						<div class="col-md-12">
							<div id="m_graph"></div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<div class="card">
								<div class="header">
									<h2>
										Monthly Detail
									</h2>
									<ul class="header-dropdown m-r--5">
										<li class="dropdown">
											<a onclick="print()" role="button" aria-haspopup="true" aria-expanded="false">
												<i class="material-icons">print</i>
											</a>
										</li>
									</ul>
								</div>
								<div class="body">
									<div class="row">
										<div class="col-md-4">
											<select onchange="m_detail($(this).val())" id="month" class="form-control">
												<?php
												foreach ($month as $key => $value) { 
													if($value == $m){ ?>
												<option selected value="<?php echo $value ?>">
													<?php echo $value.' '.$year ?></option>
												<?php }else{?>
												<option value="<?php echo $value ?>"><?php echo $value.' '.$year ?>
												</option>
												<?php } } ?>
											</select>
										</div>
									</div>

									<div class="row">
										<div class="col-md-12">
											<table class="table">
												<thead>
													<tr>
														<th id="th" width="10%">No</th>
														<th id="th" width="15%">Transaction Code</th>
														<th id="th">Member</th>
														<th id="th">Course</th>
														<th id="th">Amount</th>
													</tr>
												</thead>

												<tbody id="tbody"></tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>

<script id="monthlyjs">
	m_detail();
	$(function () {
		var chart;
		$(document).ready(function () {
			$.getJSON("report_controller/m_graph/<?php echo $year ?>",
				function (json) {

					chart = new Highcharts.Chart({
						chart: {
							renderTo: 'm_graph',
							type: 'line'

						},
						title: {
							text: 'Payment Graph <?php echo $year ?>'

						},
						subtitle: {
							text: ''

						},
						credits: {
							enabled: false
						},
						xAxis: {
							categories: <?php echo json_encode($month) ?>
						},
						yAxis: {
							title: {
								text: 'Payment Graph'
							},
							plotLines: [{
								value: 0,
								width: 1,
								color: '#808080'
							}]
						},
						tooltip: {
							formatter: function () {
								return '<b>' + this.series.name + '</b><br/>' +
									this.x + ': ' + this.y;
							}
						},
						legend: {
							layout: 'vertical',
							align: 'right',
							verticalAlign: 'top',
							x: -10,
							y: 120,
							borderWidth: 0
						},
						series: json
					});
				});

		});

	});

	function filter(year) {
		var month = $("#month").val();
		loadView('report_controller/monthly/' + month + '/' + year, '.content');
	}

	function m_detail() {
		var month = $("#month").val();
		var year = '<?php echo $year ?>';
		var url = 'report_controller/m_detail/' + month + '/' + year;
		$('#tbody').load(url);
	}

	function print() {
		var month = $("#month").val();
		window.open('m_print/' + month + '/<?php echo $year ?>');
	}

	document.getElementById('monthlyjs').innerHTML = "";
</script>