<div class="card">
	<div class="header">
		<h2>
			<a class="btn btn-primary waves-effect" style="cursor:pointer" onclick="prev()">
				<i class="material-icons">keyboard_arrow_left</i>
			</a>
			<?php echo ' Page : ' . $page . ' From ' . $all ?>
			<a class="btn btn-primary waves-effect" style="cursor:pointer" onclick="next()">
				<i class="material-icons">keyboard_arrow_right</i>
			</a>
		</h2>
	</div>
	<div class="body" style="overflow:auto;overflow-y:schroll;height:500px">

		<?php if (!empty($logs)) {?>
		<?php foreach ($logs as $log) {?>
		<div class="media">
			<div class="media-left">
				<a href="javascript:void(0);">
					<img style="border-radius:50px;" class="media-object" src="./assets/images/user.png" width="64" height="64">
				</a>
			</div>
			<div class="media-body">
				<h4 class="media-heading">
					<?php echo $log['name'] . ' : ' . to_date_time($log['created_at']) ?>
				</h4>
				<?php echo $log['log'] ?>
			</div>
		</div>
		<?php }} else {?>
			<p>No record.</p>
		<?php }?>

	</div>
</div>
<script id="logjs">
	function next()
	{
		var page = <?php echo $page + 1 ?>;
		if(page>=0){
			loadView('main_controller/log/' + page, '.log-data-all');
		}
	}
	function prev()
	{
		var page = <?php echo $page - 1 ?>;
		if(page>0){
			loadView('main_controller/log/' + page, '.log-data-all');
		}
	}
	document.getElementById('logjs').innerHTML = "";
</script>
