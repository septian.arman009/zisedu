<div class="container-fluid">
	<div class="block-header">
		<h2>WELCOME</h2>
	</div>
	<!-- Basic Table -->
	<div class="row clearfix">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="card">
				<div class="header">
					<h2>
						Zis - Education Program
						<small>Jl. KH. Agus Salim Kp. Bulak lamet No.9 Bekasi Jaya, Bekasi TImur 17112</small>
					</h2>
				</div>
				<div class="body">
					<div class="row clearfix">
						<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
							<div class="info-box bg-pink hover-expand-effect">
								<div class="icon">
									<i class="material-icons">account_circle</i>
								</div>
								<div class="content">
									<div class="text">Members</div>
									<div class="number">
										<?php echo $member ?>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
							<div class="info-box bg-orange hover-expand-effect">
								<div class="icon">
									<i class="material-icons">account_box</i>
								</div>
								<div class="content">
									<div class="text">Users</div>
									<div class="number">
										<?php echo $user ?>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
							<div class="info-box bg-green hover-expand-effect">
								<div class="icon">
									<i class="material-icons">account_box</i>
								</div>
								<div class="content">
									<div class="text">Teachers</div>
									<div class="number">
										<?php echo $teacher ?>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
							<div class="info-box bg-blue hover-expand-effect">
								<div class="icon">
									<i class="material-icons">book</i>
								</div>
								<div class="content">
									<div class="text">Courses</div>
									<div class="number">
										<?php echo $course ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="log-data-all"></div>

		</div>
	</div>
</div>

<script>
	setTimeout(() => {
		loadPartial('main_controller/log/1', '.log-data-all');
	}, 100);
</script>