<div class="container-fluid">
	<div class="block-header">
		<h2>USERS</h2>
	</div>
	<!-- Basic Table -->
	<div class="row clearfix">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="card">
				<?php if(role(['admin', 'owner'], false)){ ?>
				<div class="header">
					<h2>
						<a class="btn btn-primary waves-effect" style="cursor:pointer"
							onclick="loadView('master_controller/user','.content')">
							<i class="material-icons">keyboard_backspace</i>
						</a> User Registration Form
					</h2>
				</div>
				<?php } ?>
				<div class="body">
					<div class="row clearfix">
						<div class="col-sm-12">
							<form>
								<div class="form-group">
									<label>Name</label>
									<div class="form-line">
										<input onkeyup="form_check($(this).val())" type="text" class="form-control"
											id="name" />
									</div>
								</div>
								<div class="form-group">
									<label>Email</label>
									<div class="form-line">
										<input onkeyup="email_check($(this).val())" type="email" class="form-control"
											id="email" placeholder="example (@gmail.com)" />
									</div>
									<div style="display:none;color:red" id="invalid_email">Please type right format of
										email.</div>
									<div style="display:none;color:red" id="used">Email has been used.</div>
								</div>

								<div class="form-group">
									<label>Role</label>
									<div class="form-line">
										<select class="form-control" id="role_id">
											<?php foreach ($roles as $key => $value) { ?>
												<?php if(role(['admin', 'owner'], false)){ ?>
													<option value="<?php echo $value['role_id'] ?>"><?php echo $value['display_name'] ?></option>
												<?php }else{?>
													<?php if($value['role_id'] != 1 || $value['role_id'] != 2) {?>
														<option value="<?php echo $value['role_id'] ?>"><?php echo $value['display_name'] ?></option>
													<?php }?>
												<?php }?>
											<?php } ?>
										</select>
									</div>
								</div>
							</form>
						</div>

						<div class="col-md-2">
							<div class="form-group">
								<div class="form-line">
									<button disabled id="save" onclick="action()"
										class="btn btn-block bg-pink waves-effect">Save</button>
									<button style="display:none" id="loading"
										class="btn btn-block bg-pink waves-effect">Loading ..</button>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script id="userformjs">
	var id = '<?php echo $id ?>';
	var role = '<?php echo $role ?>';

	get_data(id);

	function get_data(id) {
		if (id != 'null') {
			data = {
				id: id,
				column: 'user_id',
				table: 'users'
			}

			postData('main_controller/get_data', data, function (err, response) {
				if (response) {
					if (response.status == 'success') {
						$("#name").val(response.data[0].name);
						$("#email").val(response.data[0].email);
						$("#role_id").val(response.data[0].role_id);
						this.v_email = 'true';
						setTimeout(() => {
							form_check();
						}, 100);
					}
				} else {
					console.log('ini error : ', err);
				}
			});
		}
	}

	function form_check(name) {
		if (name != '' && this.v_email == 'true') {
			$("#save").removeAttr('disabled');
		} else {
			$("#save").attr('disabled', 'disabled');
		}
	}

	function email_check(email) {
		if (email != '') {
			if (ValidateEmail(email, "#save", "#invalid_email")) {
				var data = {
					id: id,
					value: $("#email").val(),
					column: 'email',
					table: 'users',
					table1: 'members',
					table2: 'teachers',
				}

				postData('main_controller/triple_data_check', data, function (err, response) {
					if (response) {
						var status = response.status;
						if (status == 'success') {
							$("#used").hide();
							this.v_email = 'true';
							setTimeout(() => {
								form_check();
							}, 100);
						} else {
							$("#used").show();
							this.v_email = 'false';
							setTimeout(() => {
								form_check();
							}, 100);
						}
					} else {
						console.log('ini error : ', err);
					}
				});
			}
		} else {
			$("#invalid_email").hide();
			$("#used").hide();
		}

	}

	function action() {
		$("#save").hide();
		$("#loading").show();
		var data = {
			id: id,
			table: 'users',
			name: $('#name').val(),
			email: $('#email').val(),
			role_id: $('#role_id').val()
		}

		postData('master_controller/user_action', data, function (err, response) {
			if (response) {
				var status = response.status;
				if (status == 'success') {
					$("#save").show();
					$("#loading").hide();
					$("#name").val('');
					$("#email").val('');
					swal("Success", "User has been saved.", "success");
					if (role == 'admin') {
						loadView('master_controller/user', '.content');
					}
				} else {
					$("#save").show();
					$("#loading").hide();
					swal("Oops..!", "Save failed.", "error");
				}
			} else {
				console.log('ini error : ', err);
			}
		});
	}

	document.getElementById('userformjs').innerHTML = "";
</script>