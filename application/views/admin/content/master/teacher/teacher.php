<div class="container-fluid">
	<div class="block-header">
		<h2>TEACHERS</h2>
	</div>
	<!-- Basic Table -->
	<div class="row clearfix">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="card">
				<div class="body table-responsive">
					<div class="row">
						<div class="col-md-4">
							<select onchange="filter($(this).val())" id="status" class="form-control">
								<?php $sts = array('Need Aproval', 'Active'); foreach ($sts as $key => $value) {?>
								<?php if ($key == $status) {?>
								<option selected value="<?php echo $key ?>">
									<?php echo $value ?>
								</option>
								<?php } else {?>
								<option value="<?php echo $key ?>">
									<?php echo $value ?>
								</option>
								<?php }?>
								<?php }?>
							</select>
						</div>
						<div class="col-md-8 align-right">
							<a onclick="loadView('master_controller/form/teacher/null/<?php echo $status?>','.content')"
								class="btn btn-primary waves-effect"> Add Teacher </a>
						</div>
					</div>
					<table id="teacher-table" class="table stripe hover">
						<thead>
							<tr>
								<th id="th" width="10%">No</th>
								<th id="th">Teacher Code</th>
								<th id="th">Name</th>
								<th id="th">Address</th>
								<th id="th">Phone</th>
								<th id="th">Degree</th>
								<th id="th" class="no-sort" width="15%">Action</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th class="footer">no</th>
								<th class="footer">T Code</th>
								<th class="footer">Name</th>
								<th class="footer">Address</th>
								<th class="footer">Phone</th>
								<th class="footer">Degree</th>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
	</div>

	<script id="teacherjs">
		$(document).ready(function () {
			$('#teacher-table tfoot th').each(function () {
				var title = $(this).text();
				var inp = '<input type="text" class="form-control footer-s" id="' + title +
					'" placeholder="' + title + '" />';
				$(this).html(inp);
			});

			var table = $('#teacher-table').DataTable({
				"processing": true,
				"serverSide": true,
				"ajax": {
					"url": 'master_controller/teacher_table/<?php echo $status ?>',
					"type": "POST"
				}
			});

			table.columns().every(function () {
				var that = this;
				$('input', this.footer()).on('keyup change', function () {
					if (that.search() !== this.value) {
						that.search(this.value).draw();
					}
				});
			});
			$("#no").hide();
		});


		function destroy(id) {
			swal({
					title: "Are you sure ?",
					text: "Data will be deleted permanently.",
					type: "warning",
					showCancelButton: true,
					confirmButtonClass: "btn-danger",
					confirmButtonText: "Yes, Delete Now",
					closeOnConfirm: false
				},
				function () {
					var data = {
						id: id
					}
					postData('main_controller/destroy/teachers/id/', data, function (err, response) {
						if (response) {
							console.log('berhasil : ', response);
							if (response.status == 'success') {
								loadView('master_controller/teacher/<?php echo $status ?>', '.content');
								swal("Success", "Teacher has been deleted.", "success");
							} else {
								swal("Oops", "Delete failed.", "error");

							}
						} else {
							console.log('ini error : ', err);
						}
					});
				}
			);
		}

		function activate(id) {
			swal({
					title: "Are you sure ?",
					text: "Member will activated and you can manage Teacher Course Price",
					type: "warning",
					showCancelButton: true,
					confirmButtonClass: "btn-danger",
					confirmButtonText: "Yes, Activate Now",
					closeOnConfirm: false
				},
				function () {
					var data = {
						id: id
					}
					postData('master_controller/activate/teachers', data, function (err, response) {
						if (response) {
							console.log('berhasil : ', response);
							if (response.status == 'success') {
								loadView('master_controller/teacher/<?php echo $status ?>','.content');
								swal("Success", "Teacher has been activated.", "success");
							} else {
								swal("Oops", "Activate failed, You are now Owner.", "error");

							}
						} else {
							console.log('ini error : ', err);
						}
					});
				}
			);
		}

		function edit(id) {
			loadView('master_controller/form/teacher/' + id + '/<?php echo $status?>', '.content');
		}

		function price(id) {
			loadView('master_controller/form/price/' + id + '/<?php echo $status?>', '.content');
		}

		function filter(status) {
			loadView('master_controller/teacher/' + status, '.content');
		}

		document.getElementById('teacherjs').innerHTML = "";
	</script>

	<style>
		#teacher-table_filter {
			display: none;
		}
	</style>