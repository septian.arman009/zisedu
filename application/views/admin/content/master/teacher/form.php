<div class="container-fluid">
	<div class="block-header">
		<h2>TEACHERS</h2>
	</div>
	<!-- Basic Table -->
	<div class="row clearfix">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="card">
				<div class="header">
					<h2>
						<a class="btn btn-primary waves-effect" style="cursor:pointer"
							onclick="loadView('master_controller/teacher/<?php echo $status ?>','.content')">
							<i class="material-icons">keyboard_backspace</i>
						</a> Teacher Form
					</h2>
				</div>
				<div class="body">
					<div class="row clearfix">
						<div class="col-sm-12">
							<form>
								<div class="form-group">
									<label>Teacher Code</label>
									<div class="form-line">
										<input disabled type="text" class="form-control" id="t_code"
											value="<?php echo $t_code ?>" />
									</div>
								</div>
								<div class="form-group">
									<label>Email</label>
									<div class="form-line">
										<input onkeyup="email_check($(this).val())" type="text" class="form-control"
											id="email" placeholder="example (@gmail.com)" />
									</div>
									<div style="display:none;color:red" id="invalid_email">Please type right format of
										email.</div>
									<div style="display:none;color:red" id="used">Email has been used.</div>
								</div>
								<div class="form-group">
									<label>Name</label>
									<div class="form-line">
										<input onkeyup="form_check()" type="text" class="form-control" id="name" />
									</div>
								</div>
								<div class="form-group">
									<label>Address</label>
									<div class="form-line">
										<textarea onkeyup="form_check()" class="form-control" id="address"></textarea>
									</div>
								</div>
								<div class="form-group">
									<label>Phone</label>
									<div class="form-line">
										<input onkeyup="form_check()" type="number" class="form-control" id="phone" />
									</div>
								</div>
								<div class="form-group">
									<label>Degree</label>
									<div class="form-line">
										<input onkeyup="form_check()" type="text" class="form-control" id="degree" />
									</div>
								</div>
							</form>
						</div>

						<div class="col-md-2">
							<div class="form-group">
								<div class="form-line">
									<button disabled id="save" onclick="action()"
										class="btn btn-block bg-pink waves-effect">Save</button>
									<button style="display:none" id="loading"
										class="btn btn-block bg-pink waves-effect">Loading ..</button>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script id="teacherformjs">
	var id = '<?php echo $id ?>';

	get_data(id);

	function get_data(id) {
		if (id != 'null') {
			data = {
				id: id,
				column: 'id',
				table: 'teachers'
			}

			postData('main_controller/get_data', data, function (err, response) {
				if (response) {
					if (response.status == 'success') {
						$("#t_code").val(response.data[0].t_code);
						$("#email").val(response.data[0].email);
						$("#name").val(response.data[0].name);
						$("#address").html(response.data[0].address);
						$("#phone").val(response.data[0].phone);
						$("#degree").val(response.data[0].degree);
						this.v_email = 'true';
						setTimeout(() => {
							form_check();
						}, 100);
					}
				} else {
					console.log('ini error : ', err);
				}
			});
		}
	}

	function form_check() {
		var name = $("#name").val();
		var address = $("#address").val();
		var phone = $("#phone").val();
		var degree = $("#degree").val();
		if (name != '' && address != '' && phone != '' && degree != '' && this.v_email == 'true') {
			$("#save").removeAttr('disabled');
		} else {
			$("#save").attr('disabled', 'disabled');
		}
	}

	function email_check(email) {
		if (email != '') {
			if (ValidateEmail(email, "#save", "#invalid_email")) {
				var data = {
					id: id,
					value: email,
					column: 'email',
					table: 'teachers',
					table1: 'members',
					table2: 'users',
				}

				postData('main_controller/triple_data_check', data, function (err, response) {
					if (response) {
						var status = response.status;
						if (status == 'success') {
							$("#used").hide();
							this.v_email = 'true';
							setTimeout(() => {
								form_check();
							}, 100);
						} else {
							$("#used").show();
							this.v_email = 'false';
							setTimeout(() => {
								form_check();
							}, 100);
						}
					} else {
						console.log('ini error : ', err);
					}
				});
			}
		} else {
			$("#invalid_email").hide();
			$("#used").hide();
		}
	}

	function action() {
		$("#save").hide();
		$("#loading").show();
		var data = {
			id: id,
			table: 'teachers',
			t_code: $('#t_code').val(),
			email: $('#email').val(),
			name: $('#name').val(),
			address: $("#address").val(),
			phone: $("#phone").val(),
			degree: $("#degree").val()
		}

		postData('master_controller/teacher_action', data, function (err, response) {
			if (response) {
				var status = response.status;
				if (status == 'success') {
					$("#save").show();
					$("#loading").hide();
					swal("Success", "Teacher has been saved.", "success");
					if (id == 'null') {
						loadView('master_controller/form/teacher/null/<?php  echo $status?>', '.content');
					} else {
						loadView('master_controller/teacher/<?php echo $status ?>', '.content');
					}
				} else {
					$("#save").show();
					$("#loading").hide();
					swal("Oops..!", "Save failed.", "error");
				}
			} else {
				console.log('ini error : ', err);
			}
		});
	}

	document.getElementById('teacherformjs').innerHTML = "";
</script>