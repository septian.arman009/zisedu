<div class="container-fluid">
	<div class="block-header">
		<h2>TEACHER COURSE PRICES</h2>
	</div>
	<!-- Basic Table -->
	<div class="row clearfix">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="card">
				<div class="header">
					<h2>
						<a class="btn btn-primary waves-effect" style="cursor:pointer"
							onclick="loadView('master_controller/teacher/<?php echo $status ?>','.content')">
							<i class="material-icons">keyboard_backspace</i>
						</a>
					</h2>
				</div>
				<div class="body">
					<div class="row clearfix">
						<div class="col-sm-12">
							<table class="table">
								<thead>
									<tr>
										<th>Teacher Data</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>Teacher Code</td>
										<td>:</td>
										<td>
											<?php echo $teacher[0]['t_code'] ?>
										</td>
									</tr>
									<tr>
										<td>Name</td>
										<td>:</td>
										<td>
											<?php echo $teacher[0]['name'] ?>
										</td>
									</tr>
									<tr>
										<td>Degree</td>
										<td>:</td>
										<td>
											<?php echo $teacher[0]['degree'] ?>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="col-sm-12">

							<div class="card">
								<div class="header bg-cyan">
									<h2>
										Add Course Price <small>Choose price for each level of education.</small>
									</h2>
								</div>
								<div class="body">
									<div class="form-group">
										<label>Course Name</label>
										<div class="form-line">
											<input id="c_name" type="text" class="typehead form-control date">
										</div>
									</div>
									<div class="form-group">
										<label>Education</label>
										<div class="form-line">
											<select onchange="edu_select($(this).val())" class="form-control" id="edu">
												<option value="General">General</option>
												<option value="Primary School">Primary School</option>
												<option value="Junior High School">Junior High School</option>
												<option value="Senior High School">Senior High School</option>
												<option value="College">College</option>
											</select>
										</div>
									</div>
									<div class="form-group">
										<label id="level_name">Level</label>
										<div class="form-line">
											<select class="form-control" id="edu_level">
											</select>
										</div>
									</div>

									<div class="input-group">
										<label id="level_name">Price</label>
										<div class="form-line">
											<input id="price" type="text" class="form-control date" placeholder="Rp. 0">
										</div>
										<div class="demo-radio-button">
											<input onchange="radio('radio_1')" name="group1" type="radio" id="radio_1"
												checked>
											<label for="radio_1">Price for Level</label>
											<input onchange="radio('radio_2')" name="group1" type="radio" id="radio_2">
											<label for="radio_2">Same price for all Level</label>
											<input onchange="radio('radio_3')" name="group1" type="radio" id="radio_3">
											<label for="radio_3">Same price for all Education</label>
										</div>
										<span onclick="action()" class="input-group-addon pointer">
											Save <i class="material-icons pointer">send</i>
										</span>
									</div>
								</div>
							</div>

						</div>
						<div class="col-sm-6">
							<table class="table stripe hover">
								<thead>
									<tr>
										<th id="th">No</th>
										<th id="th">Course Code</th>
										<th id="th">Name</th>
										<th id="th"></th>
									</tr>
								</thead>
								<tbody>
									<?php $no = 1; foreach ($course as $key => $value) { ?>
									<tr>
										<td><?php echo $no++; ?></td>
										<td><?php echo $value['c_code']; ?></td>
										<td><?php echo $value['name']; ?></td>
										<td>
											<a id="b<?php echo $value['c_code']; ?>" title="Detail"
												class="btn btn-success btn-xs waves-effect"
												onclick="detail_price('<?php echo $id ?>', '<?php echo $value['c_code'] ?>');">
												<i class="material-icons">remove_red_eye</i>
											</a>
										</td>
									</tr>
									<?php } ?>
								</tbody>

							</table>
						</div>

						<div class="col-sm-6">
							<table class="table stripe hover">
								<thead>
									<tr>
										<th id="th">Member Edu</th>
										<th id="th">Edu Level</th>
										<th id="th">Price</th>
										<th id="th"></th>
									</tr>
								</thead>
								<tbody id="tbody">

								</tbody>

							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<style>
	#course_name {
		font-weight: bold;
	}

	.red {
		color: red;
	}
</style>

<script id="cpriceformjs">
	$(document).ready(function () {
		$('#price').maskMoney({
			prefix: 'Rp. ',
			thousands: '.',
			decimal: ',',
			precision: 0
		});
		edu_select($("#edu").val());
	});

	var id = '<?php echo $id ?>';

	function edu_select(edu) {
		if (edu == 'General') {
			$("#level_name").html('Level');
		} else if (edu == 'College') {
			$("#level_name").html('Semester');
		} else {
			$("#level_name").html('Class');
		}
		var edu_fix = edu.replaceAll(' ', '-');
		var url = "master_controller/edu_select/" + edu_fix;
		$('#edu_level').load(url);
	}

	$('input.typehead').typeahead({
		source: function (query, process) {
			return $.get('master_controller/search_course/<?php echo $id ?>', {
				query: query
			}, function (data) {
				data = $.parseJSON(data);
				return process(data);
			});
		}
	});

	function radio(radio) {
		if (radio == 'radio_1') {
			$("#edu").removeAttr('disabled');
			$("#edu_level").removeAttr('disabled');
		} else if (radio == 'radio_2') {
			$("#edu").removeAttr('disabled');
			$("#edu_level").attr('disabled', 'disabled');
		} else if (radio == 'radio_3') {
			$("#edu").attr('disabled', 'disabled');
			$("#edu_level").attr('disabled', 'disabled');
		}
	}

	function action() {
		$("#save").hide();
		$("#loading").show();

		if (_("radio_1").checked) {
			var radio = 'per_level';
		} else if (_("radio_2").checked) {
			var radio = 'all_level';
		} else if (_("radio_3").checked) {
			var radio = 'all_edu';
		}

		var data = {
			t_code: id,
			c_name: $("#c_name").val(),
			edu: $("#edu").val(),
			edu_level: $("#edu_level").val(),
			price: $("#price").val(),
			radio: radio
		}

		postData('master_controller/add_price', data, function (err, response) {
			if (response) {
				var status = response.status;
				if (status == 'success') {
					$("#save").show();
					$("#loading").hide();
					swal("Success", "Course price for " + $("#c_name").val() + " has been saved.", "success");
					loadView('master_controller/form/price/' + id + '/<?php echo $status ?>', '.content');
				} else {
					$("#save").show();
					$("#loading").hide();
					swal("Oops..!", "Save failed.", "error");
				}
			} else {
				console.log('ini error : ', err);
			}
		});
	}

	function detail_price(t_code, c_code) {
		$(".red").removeClass('red');
		$("#b" + c_code).addClass("red");

		var url = "master_controller/detail_price/" + t_code + '/' + c_code;
		$('#tbody').load(url);
	}

	function destroy_price(p_id, t_code, c_code) {
		swal({
				title: "Are you sure ?",
				text: "Data will be deleted permanently.",
				type: "warning",
				showCancelButton: true,
				confirmButtonClass: "btn-danger",
				confirmButtonText: "Yes, Delete Now",
				closeOnConfirm: false
			},
			function () {
				var data = {
					id: p_id
				}
				postData('main_controller/destroy/c_prices/id/', data, function (err, response) {
					if (response) {
						console.log('berhasil : ', response);
						if (response.status == 'success') {
							var url = "master_controller/detail_price/" + t_code + '/' + c_code;
							$('#tbody').load(url);
							swal("Success", "Price has been deleted.", "success");
						} else {
							swal("Oops", "Delete failed.", "error");

						}
					} else {
						console.log('ini error : ', err);
					}
				});
			}
		);
	}

	document.getElementById('cpriceformjs').innerHTML = "";
</script>