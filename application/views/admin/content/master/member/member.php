<div class="container-fluid">
	<div class="block-header">
		<h2>MEMBERS</h2>
	</div>
	<!-- Basic Table -->
	<div class="row clearfix">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="card">
				<div class="body table-responsive">
					<div class="row">
						<div class="col-md-4">
							<select onchange="filter($(this).val())" id="status" class="form-control">
								<?php $sts = array('Need Aproval', 'Active'); foreach ($sts as $key => $value) {?>
								<?php if ($key == $status) {?>
								<option selected value="<?php echo $key ?>">
									<?php echo $value ?>
								</option>
								<?php } else {?>
								<option value="<?php echo $key ?>">
									<?php echo $value ?>
								</option>
								<?php }?>
								<?php }?>
							</select>
						</div>
						<div class="col-md-8 align-right">
							<a onclick="loadView('master_controller/form/member/null/<?php echo $status?>','.content')"
								class="btn btn-primary waves-effect"> Add Member </a>
						</div>
					</div>
					<table id="member-table" class="table stripe hover">
						<thead>
							<tr>
								<th id="th" width="10%">No</th>
								<th id="th">Member Code</th>
								<th id="th">Name</th>
								<th id="th">Education</th>
								<th id="th">Place</th>
								<th id="th">Level</th>
								<th id="th" class="no-sort" width="10%">Action</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th class="footer">no</th>
								<th class="footer">M Code</th>
								<th class="footer">Name</th>
								<th class="footer">Education</th>
								<th class="footer">Place</th>
								<th class="footer">Level</th>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
	</div>

	<script id="memberjs">
		$(document).ready(function () {
			$('#member-table tfoot th').each(function () {
				var title = $(this).text();
				var inp = '<input type="text" class="form-control footer-s" id="' + title +
					'" placeholder="' + title + '" />';
				$(this).html(inp);
			});

			var table = $('#member-table').DataTable({
				"processing": true,
				"serverSide": true,
				"ajax": {
					"url": 'master_controller/member_table/<?php echo $status ?>',
					"type": "POST"
				}
			});

			table.columns().every(function () {
				var that = this;
				$('input', this.footer()).on('keyup change', function () {
					if (that.search() !== this.value) {
						that.search(this.value).draw();
					}
				});
			});
			$("#no").hide();
		});


		function destroy(id) {
			swal({
					title: "Are you sure ?",
					text: "Data will be deleted permanently.",
					type: "warning",
					showCancelButton: true,
					confirmButtonClass: "btn-danger",
					confirmButtonText: "Yes, Delete Now",
					closeOnConfirm: false
				},
				function () {
					var data = {
						id: id
					}
					postData('main_controller/destroy/members/id/', data, function (err, response) {
						if (response) {
							console.log('berhasil : ', response);
							if (response.status == 'success') {
								loadView('master_controller/member/<?php echo $status ?>', '.content');
								swal("Success", "Member has been deleted.", "success");
							} else {
								swal("Oops", "Delete failed.", "error");

							}
						} else {
							console.log('ini error : ', err);
						}
					});
				}
			);
		}

		function activate(id) {
			swal({
					title: "Are you sure ?",
					text: "Member will activated and you can fill the Member Course Reg for this member.",
					type: "warning",
					showCancelButton: true,
					confirmButtonClass: "btn-danger",
					confirmButtonText: "Yes, Activate Now",
					closeOnConfirm: false
				},
				function () {
					var data = {
						id: id
					}
					postData('master_controller/activate/members', data, function (err, response) {
						if (response) {
							console.log('berhasil : ', response);
							if (response.status == 'success') {
								loadView('master_controller/member/<?php echo $status ?>','.content');
								swal("Success", "Member has been activated.", "success");
							} else {
								swal("Oops", "Activate failed, You are now Owner.", "error");

							}
						} else {
							console.log('ini error : ', err);
						}
					});
				}
			);
		}

		function edit(id) {
			loadView('master_controller/form/member/' + id + '/<?php echo $status?>', '.content');
		}

		function filter(status) {
			loadView('master_controller/member/' + status, '.content');
		}

		document.getElementById('memberjs').innerHTML = "";
	</script>

	<style>
		#member-table_filter {
			display: none;
		}
	</style>