<div class="container-fluid">
	<div class="block-header">
		<h2>MEMBER COURSE REG</h2>
	</div>
	<!-- Basic Table -->
	<div class="row clearfix">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="card">
				<div class="header">
					<h2>
						Member Course Registration Form
					</h2>
				</div>
				<div class="body">
					<div class="row clearfix">
						<div class="col-sm-12">
							<div class="form-group">
								<label>Member Name</label>
								<div class="form-line">
									<input id="s_name" onchange="detail_member($(this).val())" type="text"
										class="typehead form-control" />
								</div>
							</div>
							<div class="form-group">
								<label>Course</label>
								<div class="form-line">
									<input id="s_course" onchange="teacher_list($(this).val())" type="text"
										class="typehead form-control" />
								</div>
							</div>
							<div class="form-group">
								<label>Teacher</label>
								<div class="form-line">
									<select onchange="teacher_change($(this).val())" id="s_teacher"
										class="form-control"></select>
								</div>
							</div>
							<div class="form-group">
								<label id="c_price"></label>
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-group">
								<div class="form-line">
									<button disabled id="save" onclick="add()"
										class="btn btn-block bg-pink waves-effect">Save</button>
									<button style="display:none" id="loading"
										class="btn btn-block bg-pink waves-effect">Loading ..</button>
								</div>
							</div>
						</div>

						<div class="col-sm-12">
							<div class="card">
								<div class="header bg-default">
									<h2>
										Member Detail
									</h2>
								</div>
								<div class="body">
									<table>
										<tbody>
											<tr>
												<td>Name</td>
												<td>:</td>
												<td id="name">-</td>
											</tr>
											<tr>
												<td>Email</td>
												<td>:</td>
												<td id="email">-</td>
											</tr>
											<tr>
												<td>Address</td>
												<td>:</td>
												<td id="address">-</td>
											</tr>
											<tr>
												<td>Education</td>
												<td>:</td>
												<td id="edu">-</td>
											</tr>
											<tr>
												<td>Level</td>
												<td>:</td>
												<td id="level">-</td>
											</tr>
											<tr>
												<td>Join Date</td>
												<td>:</td>
												<td id="join">-</td>
											</tr>
										</tbody>
									</table>

									<table class="table">
										<thead>
											<tr>
												<th id="th">No</th>
												<th id="th">Course Code</th>
												<th id="th">Course Name</th>
												<th id="th">Teacher</th>
												<th id="th">Education</th>
												<th id="th">Level</th>
												<th id="th">Price</th>
												<th id="th"></th>
											</tr>
										</thead>

										<tbody id="d_price">

										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>

<style>
	#course_name {
		font-weight: bold;
	}

	.red {
		color: red;
	}

	td {
		padding: 10px;
	}
</style>

<script id="regmemberformjs">
	$(document).ready(function () {
		$('#s_name').typeahead({
			source: function (query, process) {
				return $.get('reg_controller/search_member/', {
					query: query
				}, function (data) {
					data = $.parseJSON(data);
					return process(data);
				});
			}
		});

		$('#s_course').typeahead({
			source: function (query, process) {
				return $.get('reg_controller/search_course/', {
					query: query
				}, function (data) {
					data = $.parseJSON(data);
					return process(data);
				});
			}
		});
	});

	function teacher_list(course) {
		var edu_fix = this.edu.replaceAll(' ', '-');
		var level = this.level;
		if (course != '') {
			var course_fix = course.replaceAll(' ', '-');
		} else {
			var course_fix = 'null';
		}

		var url = "reg_controller/teacher_list/" + course_fix + "/" + edu_fix + "/" + level;
		$('#s_teacher').load(url);
		if ($("#s_teacher").val() != '' && course != 'null') {
			this.c_name = 'true';
			setTimeout(() => {
				price_check(course_fix, edu_fix, level);
				form_check();
			}, 250);
		} else {
			this.c_name = 'false';
			$("#c_price").html('');
			setTimeout(() => {
				form_check();
			}, 100);
		}
	}

	function price_check(course, edu, level) {
		var t_code = $("#s_teacher").val();
		var url = "reg_controller/price_check/" + course + "/" + edu + "/" + level + "/" + t_code;
		$('#c_price').load(url);
	}

	function teacher_change(t_code) {
		var course = $("#s_course").val().replaceAll(' ', '-');
		var edu_fix = this.edu.replaceAll(' ', '-');
		var level = this.level;
		var url = "reg_controller/price_check/" + course + "/" + edu_fix + "/" + level + "/" +
			t_code;
		$('#c_price').load(url);
	}

	function form_check() {
		if (this.c_name == 'true' && this.m_code != 'false' && $("#s_teacher") != '') {
			$("#save").removeAttr('disabled');
		} else {
			$("#save").attr('disabled', 'disabled');
		}
	}

	function detail_member(name) {
		data = {
			id: name,
			column: 'name',
			table: 'members'
		}

		postData('main_controller/get_data', data, function (err, response) {
			if (response) {
				if (response.status == 'success') {
					$("#name").html(response.data[0].name);
					$("#email").html(response.data[0].email);
					$("#address").html(response.data[0].address);
					$("#edu").html(response.data[0].edu);
					$("#level").html(response.data[0].edu_level);
					$("#join").html(to_date(response.data[0].join_date));
					this.m_code = response.data[0].m_code;
					this.edu = response.data[0].edu;
					this.level = response.data[0].edu_level;
					setTimeout(() => {
						form_check();
						course_list(response.data[0].m_code);
					}, 100);
				} else {
					$("#name").html('-');
					$("#email").html('-');
					$("#address").html('-');
					$("#edu").html('-');
					$("#level").html('-');
					$("#join").html('-');
					this.m_code = 'false';
					this.edu = 'false';
					this.level = 'false';
					setTimeout(() => {
						form_check();
					}, 100);
				}
			} else {
				console.log('ini error : ', err);
			}
		});
	}

	function course_list(m_code) {
		var url = "reg_controller/course_list/" + m_code;
		$('#d_price').load(url);
	}

	function add() {
		$("#save").hide();
		$("#loading").show();
		var name = $("#s_name").val();
		var data = {
			course: $("#s_course").val(),
			t_code: $("#s_teacher").val(),
			m_code: this.m_code,
			edu: this.edu,
			level: this.level
		}

		postData('reg_controller/add_m_course', data, function (err, response) {
			if (response) {
				var status = response.status;
				if (status == 'success') {
					$("#save").show();
					$("#loading").hide();
					$("#s_course").val('');
					detail_member(name);
					teacher_list('null');
					swal("Success", "Course has been saved.", "success");
				} else {
					$("#save").show();
					$("#loading").hide();
					swal("Oops..!", "Save failed.", "error");
				}
			} else {
				console.log('ini error : ', err);
			}
		});
	}

	function remove_course(m_code, c_code) {
		var name = $("#s_name").val();
		swal({
				title: "Are you sure ?",
				text: "Data will be deleted permanently.",
				type: "warning",
				showCancelButton: true,
				confirmButtonClass: "btn-danger",
				confirmButtonText: "Yes, Delete Now",
				closeOnConfirm: false
			},
			function () {
				var data = {
					c_code: c_code,
					m_code: m_code
				}
				postData('reg_controller/remove_course', data, function (err, response) {
					if (response) {
						console.log('berhasil : ', response);
						if (response.status == 'success') {
							$("#s_course").val('');
							detail_member(name);
							teacher_list('null');
							swal("Success", "Course has been deleted.", "success");
						} else {
							swal("Oops", "Delete failed, Member must have min 1 of course.", "error");
						}
					} else {
						console.log('ini error : ', err);
					}
				});
			}
		);
	}

	function reg_billing(m_code) {
		$("#side_img").hide();
		$("#ajax-loading").show();
		var name = $("#s_name").val();
		var data = {
			m_code: m_code
		}

		postData('reg_controller/reg_billing', data, function (err, response) {
			if (response) {
				var status = response.status;
				if (status == 'success') {
					$("#side_img").show();
					$("#ajax-loading").hide();
					$("#s_course").val('');
					detail_member(name);
					teacher_list('null');
					swal("Success", "Billing has been created.", "success");
				} else {
					$("#side_img").show();
					$("#ajax-loading").hide();
					swal("Oops..!", "Billing create failed.", "error");
				}
			} else {
				console.log('ini error : ', err);
			}
		});
	}

	document.getElementById('regmemberformjs').innerHTML = "";
</script>