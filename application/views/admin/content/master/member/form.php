<div class="container-fluid">
	<div class="block-header">
		<h2>MEMBERS</h2>
	</div>
	<!-- Basic Table -->
	<div class="row clearfix">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="card">
				<div class="header">
					<h2>
						<a class="btn btn-primary waves-effect" style="cursor:pointer"
							onclick="loadView('master_controller/member/<?php echo $status ?>','.content')">
							<i class="material-icons">keyboard_backspace</i>
						</a> Member Form
					</h2>
				</div>
				<div class="body">
					<div class="row clearfix">
						<div class="col-sm-12">
							<form>
								<div class="form-group">
									<label>Member Code</label>
									<div class="form-line">
										<input disabled type="text" class="form-control" id="m_code"
											value="<?php echo $m_code ?>" />
									</div>
								</div>
								<div class="form-group">
									<label>Email</label>
									<div class="form-line">
										<input onkeyup="email_check($(this).val())" type="text" class="form-control"
											id="email" placeholder="example (@gmail.com)" />
									</div>
									<div style="display:none;color:red" id="invalid_email">Please type right format of
										email.</div>
									<div style="display:none;color:red" id="used">Email has been used.</div>
								</div>
								<div class="form-group">
									<label>Name</label>
									<div class="form-line">
										<input onkeyup="form_check()" type="text" class="form-control" id="name" />
									</div>
								</div>
								<div class="form-group">
									<label>Address</label>
									<div class="form-line">
										<textarea onkeyup="form_check()" class="form-control" id="address"></textarea>
									</div>
								</div>
								<div class="form-group">
									<label>Education</label>
									<div class="form-line">
										<select onchange="edu_select($(this).val())" class="form-control" id="edu">
											<option value="General">General</option>
											<option value="Primary School">Primary School</option>
											<option value="Junior High School">Junior High School</option>
											<option value="Senior High School">Senior High School</option>
											<option value="College">College</option>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label id="level_name">Level</label>
									<div class="form-line">
										<select onchange="form_check()" class="form-control" id="edu_level">
										</select>
									</div>
								</div>
								<div class="form-group">
									<label>Education Place</label>
									<div class="form-line">
										<input onkeyup="form_check()" type="text" class="form-control" id="edu_place"
											placeholder="School/Campus Name" />
									</div>
								</div>
								<div class="form-group">
									<label>Join Date</label>
									<div class="form-line">
										<input onchange="form_check()" type="text"
											class="pointer datepicker form-control" id="join_date" />
									</div>
								</div>
							</form>
						</div>

						<div class="col-md-2">
							<div class="form-group">
								<div class="form-line">
									<button disabled id="save" onclick="action()"
										class="btn btn-block bg-pink waves-effect">Save</button>
									<button style="display:none" id="loading"
										class="btn btn-block bg-pink waves-effect">Loading ..</button>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script id="memberformjs">
	var id = '<?php echo $id ?>';

	$(document).ready(function () {
		$('.datepicker').bootstrapMaterialDatePicker({
			format: 'DD MMMM YYYY',
			clearButton: true,
			weekStart: 1,
			time: false
		});

		if (id == 'null') {
			edu_select($("#edu").val());
		}
	});

	function edu_select(edu) {
		if (edu == 'General') {
			$("#level_name").html('Level');
		} else if (edu == 'College') {
			$("#level_name").html('Semester');
		} else {
			$("#level_name").html('Class');
		}
		var edu_fix = edu.replaceAll(' ', '-');
		var url = "master_controller/edu_select/" + edu_fix;
		$('#edu_level').load(url);
	}

	get_data(id);

	function get_data(id) {
		if (id != 'null') {
			data = {
				id: id,
				column: 'id',
				table: 'members'
			}

			postData('main_controller/get_data', data, function (err, response) {
				if (response) {
					if (response.status == 'success') {
						$("#m_code").val(response.data[0].m_code);
						$("#email").val(response.data[0].email);
						$("#name").val(response.data[0].name);
						$("#address").html(response.data[0].address);
						edu_select(response.data[0].edu);
						setTimeout(() => {
							$("#edu").val(response.data[0].edu);
							$("#edu_level").val(response.data[0].edu_level);
						}, 100);
						$("#edu_place").val(response.data[0].edu_place);
						$("#join_date").val(to_date(response.data[0].join_date));
						this.v_email = 'true';
						setTimeout(() => {
							form_check();
						}, 100);
					}
				} else {
					console.log('ini error : ', err);
				}
			});
		}
	}

	function form_check() {
		var name = $("#name").val();
		var address = $("#address").val();
		var edu = $("#edu").val();
		var edu_place = $("#edu_place").val();
		var edu_level = $("#edu_level").val();
		var join_date = $("#join_date").val();

		if (name != '' && address != '' && edu != '' && edu_place != '' &&
			edu_level != '' && join_date != '' && this.v_email == 'true') {
			$("#save").removeAttr('disabled');
		} else {
			$("#save").attr('disabled', 'disabled');
		}

	}

	function email_check(email) {
		if (email != '') {
			if (ValidateEmail(email, "#save", "#invalid_email")) {
				var data = {
					id: id,
					value: email,
					column: 'email',
					table: 'members',
					table1: 'users',
					table2: 'teachers',
				}

				postData('main_controller/triple_data_check', data, function (err, response) {
					if (response) {
						var status = response.status;
						if (status == 'success') {
							$("#used").hide();
							this.v_email = 'true';
							setTimeout(() => {
								form_check();
							}, 100);
						} else {
							$("#used").show();
							this.v_email = 'false';
							setTimeout(() => {
								form_check();
							}, 100);
						}
					} else {
						console.log('ini error : ', err);
					}
				});
			}
		} else {
			$("#invalid_email").hide();
			$("#used").hide();
		}
	}

	function action() {
		$("#save").hide();
		$("#loading").show();
		var data = {
			id: id,
			table: 'members',
			m_code: $('#m_code').val(),
			email: $('#email').val(),
			name: $('#name').val(),
			address: $('#address').val(),
			edu: $('#edu').val(),
			edu_place: $('#edu_place').val(),
			edu_level: $('#edu_level').val(),
			join_date: $('#join_date').val()
		}

		postData('master_controller/member_action', data, function (err, response) {
			if (response) {
				var status = response.status;
				if (status == 'success') {
					$("#save").show();
					$("#loading").hide();
					swal("Success", "Member has been saved.", "success");
					if (id == 'null') {
						loadView('master_controller/form/member/null/null','.content');
					} else {
						loadView('master_controller/member/<?php echo $status ?>', '.content');
					}
				} else {
					$("#save").show();
					$("#loading").hide();
					swal("Oops..!", "Save failed.", "error");
				}
			} else {
				console.log('ini error : ', err);
			}
		});
	}

	document.getElementById('memberformjs').innerHTML = "";
</script>