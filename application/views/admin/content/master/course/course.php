<div class="container-fluid">
	<div class="block-header">
		<h2>COURSES</h2>
	</div>
	<!-- Basic Table -->
	<div class="row clearfix">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="card">
				<div class="body table-responsive">
					<a onclick="loadView('master_controller/form/course/null/null','.content')"
						class="btn btn-primary waves-effect"> Add Course </a>
					<br>
					<br>
					<table id="course-table" class="table stripe hover">
						<thead>
							<tr>
								<th id="th" width="10%">No</th>
								<th id="th">Course Code</th>
								<th id="th">Name</th>
								<th id="th" class="no-sort" width="15%">Action</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th class="footer">no</th>
								<th class="footer">C Code</th>
								<th class="footer">Name</th>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
	</div>

	<script id="coursejs">
		$(document).ready(function () {
			$('#course-table tfoot th').each(function () {
				var title = $(this).text();
				var inp = '<input type="text" class="form-control footer-s" id="' + title + '" placeholder="' + title + '" />';
				$(this).html(inp);
			});

			var table = $('#course-table').DataTable({
				"processing": true,
				"serverSide": true,
				"ajax": {
					"url": 'master_controller/course_table',
					"type": "POST"
				}
			});

			table.columns().every(function () {
				var that = this;
				$('input', this.footer()).on('keyup change', function () {
					if (that.search() !== this.value) {
						that.search(this.value).draw();
					}
				});
			});
			$("#no").hide();
		});


		function destroy(id) {
			swal({
					title: "Are you sure ?",
					text: "Data will be deleted permanently.",
					type: "warning",
					showCancelButton: true,
					confirmButtonClass: "btn-danger",
					confirmButtonText: "Yes, Delete Now",
					closeOnConfirm: false
				},
				function () {
					var data = {
						id: id
					}
					postData('main_controller/destroy/courses/id/', data, function (err, response) {
						if (response) {
							console.log('berhasil : ', response);
							if (response.status == 'success') {
								loadView('master_controller/course', '.content');
								swal("Success", "Course has been deleted.", "success");
							} else {
								swal("Oops", "Delete failed.", "error");

							}
						} else {
							console.log('ini error : ', err);
						}
					});
				}
			);
		}

		function edit(id) {
			loadView('master_controller/form/course/' + id + '/null', '.content');
		}

		function matrix(id) {
			loadView('master_controller/form/matrix/' + id + '/null', '.content');
		}

		document.getElementById('coursejs').innerHTML = "";
	</script>

	<style>
		#course-table_filter {
			display: none;
		}
	</style>