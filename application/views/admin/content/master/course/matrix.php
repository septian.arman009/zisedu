<div class="container-fluid">
	<div class="block-header">
		<h2>COURSES MATRIX</h2>
	</div>
	<!-- Basic Table -->
	<div class="row clearfix">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="card">
				<div class="header">
					<h2>
						<a class="btn btn-primary waves-effect" style="cursor:pointer"
							onclick="loadView('master_controller/course','.content')">
							<i class="material-icons">keyboard_backspace</i>
						</a>
					</h2>
				</div>
				<div class="body">
					<div class="row clearfix">
						<div class="col-sm-12">
							<table class="table">
								<thead>
									<tr>
										<th>Course Matrix</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>Course Name</td>
										<td>:</td>
										<td>
											<p id="course_name"><?php echo $course[0]['name'] ?><p>
										</td>
									</tr>
									<tr>
										<td>Add Teacher</td>
										<td>:</td>
										<td>
											<div class="input-group">
												<div class="form-line">
													<input id="name" type="text" class="typehead form-control date"
														placeholder="Teacher Name">
												</div>
												<span onclick="add()" class="input-group-addon pointer">
													Save <i class="material-icons pointer">send</i>
												</span>
											</div>
										</td>
									</tr>
								</tbody>

							</table>
						</div>

						<div class="col-sm-12">
							<table class="table stripe hover">
								<thead>
									<tr>
										<th id="th">No</th>
										<th id="th">Teacher Code</th>
										<th id="th">Name</th>
										<th id="th">Phone</th>
										<th id="th">Degree</th>
										<th id="th"></th>
									</tr>
								</thead>
								<tbody>
									<?php $no = 1; foreach ($teacher as $key => $value) { ?>
										<tr>
											<td><?php echo $no++; ?></td>
											<td><?php echo $value['t_code'] ?></td>
											<td><?php echo $value['name'] ?></td>
											<td><?php echo $value['phone'] ?></td>
											<td><?php echo $value['degree'] ?></td>
											<td>
												<a onclick="remove_matrix('<?php echo $id ?>', '<?php echo $value['t_code'] ?>')" 
												title="Delete" class="btn btn-danger btn-xs waves-effect" ><i class="material-icons">delete</i></a>
											</td>
										</tr>
									<?php } ?>
								</tbody>

							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<style>
	#course_name {
		font-weight: bold;
	}
</style>

<script id="matrixformjs">
	$('input.typehead').typeahead({
		source: function (query, process) {
			return $.get('master_controller/search_teacher/<?php echo $id ?>', {
				query: query
			}, function (data) {
				data = $.parseJSON(data);
				return process(data);
			});
		}
	});

	function add() {
		var data = {
			c_code: '<?php echo $id ?>',
			teacher:  $("#name").val()
		}

		postData('master_controller/add_matrix', data, function (err, response) {
			if (response) {
				var status = response.status;
				if (status == 'success') {
					swal("Success", "Teacher has been added to matrix.", "success");
					loadView('master_controller/form/matrix/<?php echo $id ?>/null', '.content');
				} else {
					swal("Oops..!", "Add teacher failed.", "error");
				}
			} else {
				console.log('ini error : ', err);
			}
		});
	}

	function remove_matrix(c_code, t_code) {
			swal({
					title: "Are you sure ?",
					text: "Data will be deleted permanently.",
					type: "warning",
					showCancelButton: true,
					confirmButtonClass: "btn-danger",
					confirmButtonText: "Yes, Delete Now",
					closeOnConfirm: false
				},
				function () {
					var data = {
						c_code: c_code,
						t_code: t_code
					}
					postData('master_controller/remove_matrix', data, function (err, response) {
						if (response) {
							if (response.status == 'success') {
								swal("Success", "Teacher has been deleted from matrix.", "success");
								loadView('master_controller/form/matrix/<?php echo $id ?>', '.content');
							} else {
								swal("Oops", "Delete failed.", "error");
							}
						} else {
							console.log('ini error : ', err);
						}
					});
				}
			);
		}

	document.getElementById('matrixformjs').innerHTML = "";
</script>