<div class="container-fluid">
	<div class="block-header">
		<h2>COURSES</h2>
	</div>
	<!-- Basic Table -->
	<div class="row clearfix">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="card">
				<div class="header">
					<h2>
						<a class="btn btn-primary waves-effect" style="cursor:pointer"
							onclick="loadView('master_controller/course','.content')">
							<i class="material-icons">keyboard_backspace</i>
						</a> Course Form
					</h2>
				</div>
				<div class="body">
					<div class="row clearfix">
						<div class="col-sm-12">
							<form>
								<div class="form-group">
									<label>Cource Code</label>
									<div class="form-line">
										<input disabled type="text" class="form-control" id="c_code"
											value="<?php echo $c_code ?>" />
									</div>
								</div>
								<div class="form-group">
									<label>Name</label>
									<div class="form-line">
										<input onkeyup="form_check()" type="text" class="form-control" id="name" />
									</div>
									<div style="display:none;color:red" id="used">Course name has been used.</div>
								</div>
							</form>
						</div>

						<div class="col-md-2">
							<div class="form-group">
								<div class="form-line">
									<button disabled id="save" onclick="action()"
										class="btn btn-block bg-pink waves-effect">Save</button>
									<button style="display:none" id="loading"
										class="btn btn-block bg-pink waves-effect">Loading ..</button>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script id="courseformjs">
	var id = '<?php echo $id ?>';

	get_data(id);

	function get_data(id) {
		if (id != 'null') {
			data = {
				id: id,
				column: 'id',
				table: 'courses'
			}

			postData('main_controller/get_data', data, function (err, response) {
				if (response) {
					if (response.status == 'success') {
						$("#c_code").val(response.data[0].c_code);
						$("#name").val(response.data[0].name);
						form_check();
					}
				} else {
					console.log('ini error : ', err);
				}
			});
		}
	}

	function form_check() {
		var name = $("#name").val();
		if (name != '') {
			name_check();
		} else {
			$("#used").hide();
			$("#save").attr('disabled', 'disabled');
		}
	}

	function name_check() {

		var data = {
			id: id,
			value: $("#name").val(),
			column: 'name',
			table: 'courses',
		}

		postData('main_controller/single_data_check', data, function (err, response) {
			if (response) {
				var status = response.status;
				if (status == 'success') {
					$("#used").hide();
					$("#save").removeAttr('disabled');
				} else {
					$("#used").show();
					$("#save").attr('disabled', 'disabled');
				}
			} else {
				console.log('ini error : ', err);
			}
		});
	}

	function action() {
		$("#save").hide();
		$("#loading").show();
		var data = {
			id: id,
			table: 'courses',
			c_code: $('#c_code').val(),
			name: $('#name').val()
		}

		postData('master_controller/course_action', data, function (err, response) {
			if (response) {
				var status = response.status;
				if (status == 'success') {
					$("#save").show();
					$("#loading").hide();
					swal("Success", "Course has been saved.", "success");
					if (id == 'null') {
						loadView('master_controller/form/course/null/null', '.content');
					} else {
						loadView('master_controller/course', '.content');
					}
				} else {
					$("#save").show();
					$("#loading").hide();
					swal("Oops..!", "Save failed.", "error");
				}
			} else {
				console.log('ini error : ', err);
			}
		});
	}

	document.getElementById('courseformjs').innerHTML = "";
</script>