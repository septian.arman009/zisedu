<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Main_controller extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('main_model');
        $this->perPage = 10;
        auth();
    }

    public function error_handling()
    {
        $this->load->view('404');
    }

    public function index()
    {
        if (!isset($_COOKIE['theme_koperasi'])) {
            $data['theme'] = 'theme-cyan';
        } else {
            $data['theme'] = $_COOKIE['theme_koperasi'];
        }

        if ($data['theme'] == 'theme-red') {
            $data['table_color'] = '#F44336';
        } else if ($data['theme'] == 'theme-pink') {
            $data['table_color'] = '#E91E63 ';
        } else if ($data['theme'] == 'theme-purple') {
            $data['table_color'] = '#9C27B0 ';
        } else if ($data['theme'] == 'theme-deep-purple') {
            $data['table_color'] = '#673AB7 ';
        } else if ($data['theme'] == 'theme-indigo') {
            $data['table_color'] = '#3F51B5 ';
        } else if ($data['theme'] == 'theme-blue') {
            $data['table_color'] = '#2196F3 ';
        } else if ($data['theme'] == 'theme-light-blue') {
            $data['table_color'] = '#03A9F4 ';
        } else if ($data['theme'] == 'theme-cyan') {
            $data['table_color'] = '#00BCD4 ';
        } else if ($data['theme'] == 'theme-teal') {
            $data['table_color'] = '#009688';
        } else if ($data['theme'] == 'theme-green') {
            $data['table_color'] = '#4CAF50 ';
        } else if ($data['theme'] == 'theme-light-green') {
            $data['table_color'] = '#8BC34A ';
        } else if ($data['theme'] == 'theme-lime') {
            $data['table_color'] = '#CDDC39 ';
        } else if ($data['theme'] == 'theme-yellow') {
            $data['table_color'] = '#ffe821 ';
        } else if ($data['theme'] == 'theme-amber') {
            $data['table_color'] = '#FFC107 ';
        } else if ($data['theme'] == 'theme-orange') {
            $data['table_color'] = '#FF9800 ';
        } else if ($data['theme'] == 'theme-deep-orange') {
            $data['table_color'] = '#FF5722 ';
        } else if ($data['theme'] == 'theme-brown') {
            $data['table_color'] = '#795548 ';
        } else if ($data['theme'] == 'theme-grey') {
            $data['table_color'] = '#9E9E9E ';
        } else if ($data['theme'] == 'theme-blue-grey') {
            $data['table_color'] = '#607D8B ';
        } else if ($data['theme'] == 'theme-black') {
            $data['table_color'] = '#000000 ';
        }
        $data['setting'] = $this->main_model->gda1p('settings');
        $data['date_now'] = explode('-', date('Y-m-d'));
        $this->load->view('admin/content/main', $data);
    }

    public function home()
    {
        $data['member'] = $this->main_model->count('members', 'id');
        $data['user'] = $this->main_model->count('users', 'user_id');
        $data['teacher'] = $this->main_model->count('teachers', 'id');
        $data['course'] = $this->main_model->count('courses', 'id');
        $this->load->view('admin/content/home/home', $data);
    }

    public function log($page)
    {
        $total_row = $this->main_model->count('logs', 'id');
        $data['all'] = ceil($total_row / 10);
        if ($page != 1) {
            $end = $this->perPage * $page;
            $start = $end - 10;
            $data['page'] = $page;
            if ($start > 0) {
                $data['logs'] = $this->main_model->limit($start, $end, 'logs');
                $this->load->view('admin/content/home/log', $data);
            } else {
                $data['logs'] = '';
                $this->load->view('admin/content/home/log', $data);
            }
        } else {
            $data['page'] = $page;
            $data['logs'] = $this->main_model->limit(0, $this->perPage, 'logs');
            $this->load->view('admin/content/home/log', $data);
        }
    }

    public function destroy($table, $colom)
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $id = $obj->id;
        if ($table == 'users' && $id == $_SESSION['bim_in']['id']) {
        } else if (role(['owner', 'admin'], true)) {
            if ($table == 'teachers') {
                $t_code = $this->main_model->gdo4p('teachers', 't_code', 'id', $id);
                $this->main_model->destroy('c_matrixs', 't_code', $t_code);
            } 
            $this->main_model->destroy($table, $colom, $id);
            r_success();
        }
    }

    public function logout()
    {
        $this->session->unset_userdata('bim_in');
    }

    public function setTheme($theme)
    {
        $cookie = array(
            "name" => 'theme_koperasi',
            "value" => "$theme",
            "expire" => 7200,
            "secure" => false,
        );
        $this->input->set_cookie($cookie);
    }

    public function max_member()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $data['max_member'] = $obj->max_member;
        $update = $this->main_model->update('settings', $data, 'id', 1);
        if ($update) {
            r_success();
        }
    }

    public function turn_email_sender()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $data['email'] = $obj->checkbox_email;
        $update = $this->main_model->update('settings', $data, 'id', 1);
        if ($update) {
            r_success();
        }
    }

    public function max_payment_date()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $data['max_payment_date'] = $obj->max_payment_date;
        $update = $this->main_model->update('settings', $data, 'id', 1);
        if ($update) {
            r_success();
        }
    }

    public function update_sys_mail()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $data['send_mail'] = $obj->email;
        $data['send_pass'] = $obj->password;
        if (role(['admin', 'owner'], true)) {
            $update = $this->main_model->update('settings', $data, 'id', 1);
            if ($update) {
                r_success();
            }
        }
    }

    public function exist_check()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $value = $obj->value;
        $column = $obj->column;
        $table = $obj->table;

        $check = $this->main_model->gda3p($table, $column, $value);
        if ($check) {
            r_success();
        }
    }

    public function single_data_check()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $id = $obj->id;
        $value = $obj->value;
        $column = $obj->column;
        $table = $obj->table;
        if ($id == 'null') {
            $check = $this->main_model->gda3p($table, $column, $value);
            if (!$check) {
                r_success();
            }
        } else {
            $old = $this->main_model->gdo4p($table, $column, 'id', $id);
            if ($value == $old) {
                r_success();
            } else {
                $check = $this->main_model->gda3p($table, $column, $value);
                if (!$check) {
                    r_success();
                }
            }
        }
    }

    public function triple_data_check()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $id = $obj->id;
        $value = $obj->value;
        $column = $obj->column;
        $table = $obj->table;
        $table1 = $obj->table1;
        $table2 = $obj->table2;
        if ($id == 'null') {
            $check = $this->main_model->gda3p($table, $column, $value);
            $check1 = $this->main_model->gda3p($table1, $column, $value);
            $check2 = $this->main_model->gda3p($table2, $column, $value);
            if (!$check && !$check1 && !$check2) {
                r_success();
            }
        } else {
            $old = $this->main_model->gdo4p($table, $column, 'id', $id);
            if ($value == $old) {
                r_success();
            } else {
                $check = $this->main_model->gda3p($table, $column, $value);
                $check1 = $this->main_model->gda3p($table1, $column, $value);
                $check2 = $this->main_model->gda3p($table2, $column, $value);
                if (!$check && !$check1 && !$check2) {
                    r_success();
                }
            }
        }
    }

    public function get_data()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $id = $obj->id;
        $column = $obj->column;
        $table = $obj->table;
        $data = $this->main_model->gda3p($table, $column, $id);
        if ($data) {
            r_success_data($data);
        }
    }

    public function search_ajax($column, $table)
    {
        $query = $this->input->get('query');
        $this->db->like($column, $query);
        $data = $this->db->get($table)->result();
        echo json_encode($data);
    }

}
