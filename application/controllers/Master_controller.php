<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Master_controller extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('main_model');
        auth();
    }

    public function form($form, $id, $param)
    {
        $data['id'] = $id;
        if ($form == 'user') {
            $data['role'] = $_SESSION['bim_in']['role'];
            $data['roles'] = $this->main_model->gda1p('roles');
            $this->load->view('admin/content/master/user/form', $data);
        } else if ($form == 'course') {
            $data['c_code'] = generate_code('C', 'courses');
            $this->load->view('admin/content/master/course/form', $data);
        } else if ($form == 'matrix') {
            $data['course'] = $this->main_model->gda3p('courses', 'c_code', $id);
            $data['teacher'] = $this->main_model->teacher_list($id);
            $this->load->view('admin/content/master/course/matrix', $data);
        } else if ($form == 'teacher') {
            $data['status'] = $param;
            $data['t_code'] = generate_code('T', 'teachers');
            $this->load->view('admin/content/master/teacher/form', $data);
        } else if ($form == 'price') {
            $data['status'] = $param;
            $data['teacher'] = $this->main_model->gda3p('teachers', 't_code', $id);
            $data['course'] = $this->main_model->course_list($id);
            $this->load->view('admin/content/master/teacher/c_price', $data);
        } else if ($form == 'member') {
            $data['status'] = $param;
            $data['m_code'] = generate_code('M', 'members');
            $this->load->view('admin/content/master/member/form', $data);
        }
    }

    public function user()
    {
        if (role(['admin', 'owner'], false)) {
            $this->load->view('admin/content/master/user/user');
        } else {
            $this->load->view('403');
        }
    }

    public function user_table()
    {
        $id = 'user_id';
        $table = 'users';
        $column = array(
            'user_id',
            'name',
            'email',
        );
        show_table($id, $table, $column, 'null');
    }

    public function course()
    {
        if (role(['admin', 'user', 'owner'], false)) {
            $this->load->view('admin/content/master/course/course');
        } else {
            $this->load->view('403');
        }
    }

    public function course_table()
    {
        $id = 'id';
        $table = 'courses';
        $column = array(
            'id',
            'c_code',
            'name',
        );
        show_table($id, $table, $column, 'null');
    }

    public function teacher($status)
    {
        if (role(['admin', 'user', 'owner'], false)) {
            $data['status'] = $status;
            $this->load->view('admin/content/master/teacher/teacher', $data);
        } else {
            $this->load->view('403');
        }
    }

    public function teacher_table($status)
    {
        $id = 'id';
        $table = 'teachers';
        $column = array(
            'id',
            't_code',
            'name',
            'address',
            'phone',
            'degree',
        );
        show_table($id, $table, $column, $status);
    }

    public function member($status)
    {
        if (role(['admin', 'user', 'owner'], false)) {
            $data['status'] = $status;
            $this->load->view('admin/content/master/member/member', $data);
        } else {
            $this->load->view('403');
        }
    }

    public function member_table($status)
    {
        $id = 'id';
        $table = 'members';
        $column = array(
            'id',
            'm_code',
            'name',
            'edu',
            'edu_place',
            'edu_level',
        );
        show_table($id, $table, $column, $status);
    }

    public function course_action()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $id = $obj->id;
        $table = $obj->table;
        $data['c_code'] = $obj->c_code;
        $data['name'] = ucwords($obj->name);

        if ($id == 'null') {
            $message = "Menambahkan course baru : {$data['name']}";
        } else {
            $message = "Mengubah course dengan code : {$data['c_code']} menjadi : {$data['name']}";
        }

        do_action($id, $table, 'id', $data, $message);
    }

    public function user_action()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $id = $obj->id;
        $table = $obj->table;
        $data['name'] = ucwords($obj->name);
        $data['email'] = $obj->email;
        $data['role_id'] = $obj->role_id;

        if ($id == 'null') {
            $password = random_string(8);
            $data['password'] = md5($password);
            $message = "Random password for {$data['email']} : {$password}";
            $subject = "Random Password Zis - EDU";
            $send_mail = send_mail($data['email'], $message, $subject);

            $log_message = "Mendaftarkan user baru dengan email : {$data['email']} 
            cek email untuk melihat password yang sudah dikirimkan";

            if ($send_mail) {
                do_action($id, $table, 'user_id', $data, $log_message);
            }
        } else {
            $log_message = "Update user dengan email : {$data['email']}";
            do_action($id, $table, 'user_id', $data, $log_message);
        }
    }

    public function teacher_action()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $id = $obj->id;
        $table = $obj->table;
        $data['t_code'] = $obj->t_code;
        $data['email'] = $obj->email;
        $data['name'] = ucwords($obj->name);
        $data['address'] = ucwords($obj->address);
        $data['phone'] = $obj->phone;
        $data['degree'] = ucwords($obj->degree);
        
        if ($id == 'null') {
            $data['status'] = 0;
            $message = "Menambahkan teacher baru : {$data['name']}";
        } else {
            $message = "Mengubah teacher dengan code : {$data['t_code']}";
        }

        do_action($id, $table, 'id', $data, $message);
    }

    public function member_action()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $id = $obj->id;
        $table = $obj->table;
        $data['m_code'] = $obj->m_code;
        $data['email'] = $obj->email;
        $data['name'] = ucwords($obj->name);
        $data['address'] = ucwords($obj->address);
        $data['edu'] = $obj->edu;
        $data['edu_place'] = ucwords($obj->edu_place);
        $data['edu_level'] = ucwords($obj->edu_level);
        $data['join_date'] = to_date_mysql($obj->join_date);

        if ($id == 'null') {
            $data['status'] = 0;
            $message = "Menambahkan member baru : {$data['name']}";
        } else {
            $message = "Mengubah member dengan code : {$data['m_code']}";
        }

        do_action($id, $table, 'id', $data, $message);
    }

    public function search_teacher($c_code)
    {
        $name = $this->input->get('query');
        $data = $this->db->query("select name from teachers where name like '%$name%' and t_code not in((select t_code from c_matrixs where c_code = '$c_code'))")->result();
        echo json_encode($data);
    }

    public function search_course($t_code)
    {
        $name = $this->input->get('query');
        $data = $this->db->query("select name from courses where name like '%$name%' and c_code in((select c_code from c_matrixs where t_code = '$t_code'))")->result();
        echo json_encode($data);
    }

    public function add_matrix()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $data['c_code'] = $obj->c_code;
        $teacher = $obj->teacher;
        $data['t_code'] = $this->main_model->gdo4p('teachers', 't_code', 'name', $teacher);
        $course = $this->main_model->gdo4p('courses', 'name', 'c_code', $data['c_code']);

        $message = "Menambahkan teacher : {$teacher} ke matrix course : {$course}";

        $store = $this->main_model->store('c_matrixs', $data);
        if ($store) {
            logs($message);
            r_success();
        }
    }

    public function remove_matrix()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $c_code = $obj->c_code;
        $t_code = $obj->t_code;
        $teacher = $this->main_model->gdo4p('teachers', 'name', 't_code', $t_code);
        $course = $this->main_model->gdo4p('courses', 'name', 'c_code', $c_code);
        $message = "Menghpaus teacher : {$teacher} dari matrix course : {$course}";
        $delete = $this->main_model->destroy2p('c_matrixs', 'c_code', $c_code, 't_code', $t_code);
        if ($delete) {
            $this->main_model->destroy2p('c_prices', 't_code', $t_code, 'c_code', $c_code);
            logs($message);
            r_success();
        }
    }

    public function edu_select($edu)
    {
        $data = '';

        $value = str_replace('-', ' ', $edu);

        if ($value == 'General') {
            $level = array('General');
        } else if ($value == 'Primary School') {
            $level = array('1', '2', '3', '4', '5', '6');
        } else if ($value == 'Junior High School') {
            $level = array('7', '8', '9');
        } else if ($value == 'Senior High School') {
            $level = array('10', '11', '12');
        } else if ($value == 'College') {
            $level = array('1', '2', '3', '4', '5', '6', '7', '8');
        }

        foreach ($level as $key => $value) {
            $data .= "<option value='" . $value . "'>" . $value . "</option>";
        }

        echo $data;
    }

    public function add_price()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $data['t_code'] = $obj->t_code;
        $data['c_code'] = $this->main_model->gdo4p('courses', 'c_code', 'name', $obj->c_name);
        $data['price'] = to_int($obj->price);
        $radio = $obj->radio;

        if ($data['c_code'] && $data['price'] != '') {

            if ($radio == 'per_level') {
                $data['edu'] = $obj->edu;
                $data['edu_level'] = $obj->edu_level;
                $message = "Menambahkan course price : {$data['c_code']} untuk teacher : {$data['t_code']}";
                $check = $this->main_model->gda9p('c_prices', 't_code', $data['t_code'], 'c_code', $data['c_code'], 'edu', $data['edu'], 'edu_level', $data['edu_level']);
                if (!$check) {
                    $save = $this->main_model->store('c_prices', $data);
                } else {
                    $save = $this->main_model->update10p('c_prices', $data, 't_code', $data['t_code'], 'c_code', $data['c_code'], 'edu', $data['edu'], 'edu_level', $data['edu_level']);
                }
                if ($save) {
                    logs($message);
                    r_success();
                }
            } else if ($radio == 'all_level') {
                $data['edu'] = $obj->edu;
                if ($data['edu'] == 'General') {
                    $level = array('General');
                } else if ($data['edu'] == 'Primary School') {
                    $level = array('1', '2', '3', '4', '5', '6');
                } else if ($data['edu'] == 'Junior High School') {
                    $level = array('7', '8', '9');
                } else if ($data['edu'] == 'Senior High School') {
                    $level = array('10', '11', '12');
                } else if ($data['edu'] == 'College') {
                    $level = array('1', '2', '3', '4', '5', '6', '7', '8');
                }

                $message = "Menambahkan course price same for all Level to : {$data['c_code']} untuk teacher : {$data['t_code']}";
                foreach ($level as $key => $value) {
                    $data['edu_level'] = $value;
                    $check = $this->main_model->gda9p('c_prices', 't_code', $data['t_code'], 'c_code', $data['c_code'], 'edu', $data['edu'], 'edu_level', $value);
                    if (!$check) {
                        $save = $this->main_model->store('c_prices', $data);
                    } else { 
                        $save = $this->main_model->update10p('c_prices', $data, 't_code', $data['t_code'], 'c_code', $data['c_code'], 'edu', $data['edu'], 'edu_level', $value);
                    }
                }
                logs($message);
                r_success();
            } else if ($radio == 'all_edu') {
                $edu = array(
                    'General',
                    'Primary School',
                    'Junior High School',
                    'Senior High School',
                    'College',
                );
                $level = array(
                    array('General'),
                    array('1', '2', '3', '4', '5', '6'),
                    array('7', '8', '9'),
                    array('10', '11', '12'),
                    array('1', '2', '3', '4', '5', '6', '7', '8'),
                );

                $message = "Menambahkan course price same for all Level to : {$data['c_code']} untuk teacher : {$data['t_code']}";
                $index = 0;
                foreach ($edu as $key => $value) {
                    foreach ($level[$index] as $k => $v) {
                        $data['edu'] = $value;
                        $data['edu_level'] = $v;
                        $check = $this->main_model->gda9p('c_prices', 't_code', $data['t_code'], 'c_code', $data['c_code'], 'edu', $value, 'edu_level', $v);
                        if (!$check) {
                            $save = $this->main_model->store('c_prices', $data);
                        } else {
                            $save = $this->main_model->update10p('c_prices', $data, 't_code', $data['t_code'], 'c_code', $data['c_code'], 'edu', $data['edu'], 'edu_level', $v);
                        }
                    }
                    $index++;
                }

                logs($message);
                r_success();
            }
        }
    }

    public function detail_price($t_code, $c_code)
    {
        $data = '';
        $detail = $this->main_model->gda5po('c_prices', 't_code', $t_code, 'c_code', $c_code, 'edu');
        foreach ($detail as $key => $value) {
            $level = level($value['edu'], $value['edu_level']);
            $button =
                '<a title="Delete" class="btn btn-danger btn-xs waves-effect"
                onclick="destroy_price(' .$value['id']. ',' . "'" . $t_code . "'" . ',' . "'" . $c_code . "'" . ');"">
                <i class="material-icons">delete</i></a>';

            $data .= '<tr> <td>' . $value['edu'] . '</td> <td>' . $level . '</td> <td>' . torp($value['price']) . '</td> <td>' . $button . '</td> </tr>';
        }
        echo $data;
    }

    public function activate($table)
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $id = $obj->id;
        $data['status'] = 1;

        if(role(['owner'], true)){
            $update = $this->main_model->update($table, $data, 'id', $id);
            if($update){
                r_success();
            }
        }
    }
}
