<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Report_controller extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('main_model');
        auth();
    }

    public function monthly($month, $year)
    {
        if (role(['admin', 'owner'], false)) {
            $data['year'] = $year;
            $data['m'] = to_month($month);
            $data['month'] = array(
                'January',
                'February',
                'March',
                'April',
                'May',
                'June',
                'July',
                'August',
                'September',
                'October',
                'November',
                'December'
            );
            $this->load->view('admin/content/payment/report/monthly', $data);
        } else {
            $this->load->view('403');
        }
    }

    public function m_graph($year)
    {
        
        $billing = $this->main_model->gda3p('bills', 'period', $year);

        $jan = 0;
        $feb = 0;
        $mar = 0;
        $april = 0;
        $may = 0;
        $jun = 0;
        $jul = 0;
        $aug = 0;
        $sep = 0;
        $oct = 0;
        $nov = 0;
        $dec = 0;
        
        foreach ($billing as $key => $vb) {
            $payment = unserialize($vb['payment']);
            for ($i=1; $i <= 12 ; $i++) { 
                if($payment[$i]['month'] == 'January'){
                    $jan += $payment[$i]['price'];
                }else if($payment[$i]['month'] == 'February'){
                    $feb += $payment[$i]['price'];
                }else if($payment[$i]['month'] == 'March'){
                    $mar += $payment[$i]['price'];
                }else if($payment[$i]['month'] == 'April'){
                    $april += $payment[$i]['price'];
                }else if($payment[$i]['month'] == 'May'){
                    $may += $payment[$i]['price'];
                }else if($payment[$i]['month'] == 'June'){
                    $jun += $payment[$i]['price'];
                }else if($payment[$i]['month'] == 'July'){
                    $jul += $payment[$i]['price'];
                }else if($payment[$i]['month'] == 'August'){
                    $aug += $payment[$i]['price'];
                }else if($payment[$i]['month'] == 'September'){
                    $sep += $payment[$i]['price'];
                }else if($payment[$i]['month'] == 'October'){
                    $oct += $payment[$i]['price'];
                }else if($payment[$i]['month'] == 'November'){
                    $nov += $payment[$i]['price'];
                }else if($payment[$i]['month'] == 'December'){
                    $dec += $payment[$i]['price'];
                }
            }
        }

        $total = array($jan, $feb, $mar, $april, $may, $jun, $jul, $aug, $sep, $oct, $nov, $dec);

        foreach ($total as $key => $value) {
            $row['data'][] = $value;
        }

        $row['name'] = 'Payment';
        $result = array();
        array_push($result, $row);
        print json_encode($result, JSON_NUMERIC_CHECK);
    }

    public function m_detail($month, $year)
    {
        $m_index = to_m($month);
        $billing = $this->main_model->gda3p('bills', 'period', $year);
        $total = 0;
        $transaction = array();
        foreach ($billing as $key => $vb) {
            $payment = unserialize($vb['payment']);
            if($payment[$m_index]['tc_code'] != ''){
                $transaction[] = array(
                    'tc_code' => $payment[$m_index]['tc_code'],
                    'm_name' => $this->main_model->gdo4p('members', 'name', 'm_code', $vb['m_code']),
                    'course' => $payment[$m_index]['course'],
                    'amount' => $payment[$m_index]['price']
                );
                $total += $payment[$m_index]['price'];
            }
        }

        $data['transaction'] = $transaction;
        $data['total'] = $total;

        if($transaction){
            $this->load->view('admin/content/payment/report/m_detail', $data);
        }
    }

    public function m_print($month, $year)
    {   
        $m_index = to_m($month);
        $billing = $this->main_model->gda3p('bills', 'period', 2019);
        $total = 0;
        $transaction = array();
        foreach ($billing as $key => $vb) {
            $payment = unserialize($vb['payment']);
            if($payment[$m_index]['tc_code'] != ''){
                $transaction[] = array(
                    'tc_code' => $payment[$m_index]['tc_code'],
                    'm_name' => $this->main_model->gdo4p('members', 'name', 'm_code', $vb['m_code']),
                    'course' => $payment[$m_index]['course'],
                    'amount' => $payment[$m_index]['price']
                );
                $total += $payment[$m_index]['price'];
            }
        }

        $data['transaction'] = $transaction;
        $data['total'] = $total;
        $data['month'] = $month;
        $data['year'] = $year;
        
        $this->load->library('fpdf');
        $this->load->view('admin/content/payment/report/m_print', $data);
    }

}
