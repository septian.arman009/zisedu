<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Payment_controller extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('main_model');
        auth();
    }

    public function billing($year)
    {
        if (role(['admin', 'user', 'owner'], false)) {
            $data['year'] = $year;
            $this->load->view('admin/content/payment/billing/billing', $data);
        } else {
            $this->load->view('403');
        }
    }

    public function billing_table($year)
    {
        $id = 'id';
        $table = 'bills';
        $column = array(
            'id',
            'b_code',
            'm_code',
            'm_code',
            'period',
        );
        show_table($id, $table, $column, $year);
    }

    public function billing_detail($id, $year)
    {
        $billing = $this->main_model->gda3p('bills', 'id', $id);
        $data['payment'] = unserialize($billing[0]['payment']);
        $data['member'] = $this->main_model->gda3p('members', 'm_code', $billing[0]['m_code']);
        $data['id'] = $id;
        $data['year'] = $year;
        $this->load->view('admin/content/payment/billing/billing_detail', $data);
    }

    public function pay_form($id, $month, $year)
    {
        $billing = $this->main_model->gda3p('bills', 'id', $id);
        $data['id'] = $id;
        $data['month'] = $month;
        $data['year'] = $year;
        $payment = unserialize($billing[0]['payment']);
        $data['index'] = arraySearch($payment, 'month', $month);
        $data['payment_date'] = $payment[$data['index']]['payment_date'];
        $data['img'] = $payment[$data['index']]['img'];
        $data['m_name'] = $this->main_model->gdo4p('members', 'name', 'm_code', $billing[0]['m_code']);
        $data['m_code'] = $billing[0]['m_code'];
        $m_course = unserialize($this->main_model->gdo4p('m_courses', 'course', 'm_code', $billing[0]['m_code']));
        if($data['payment_date'] == ''){
            $data['amount'] = array_sum(array_column($m_course, 'price'));
        }else{
            $data['amount'] = $payment[$data['index']]['price'];
        }
        $this->load->view('admin/content/payment/billing/pay_form', $data);
    }

    public function pay()
    {
        $id = $this->input->post('id');
        $index = $this->input->post('index');
        $amount = to_int($this->input->post('amount'));
        $payment_date = to_date_mysql($this->input->post('payment_date'));

        $billing = $this->main_model->gda3p('bills', 'id', $id);
        $payment = unserialize($billing[0]['payment']);

        if (!empty($_FILES)) {
            $old_img = $payment[$index]['img'];
            $config['upload_path'] = "./assets/bill_img/";
            $config['allowed_types'] = 'jpg|png|jpeg';

            $filename = $_FILES["file"]["name"];
            $file_ext = pathinfo($filename, PATHINFO_EXTENSION);
            $new_name = $billing[0]['b_code'] . '.' . $file_ext;
            $config['file_name'] = $new_name;

            $this->load->library('upload', $config);
            $this->upload->do_upload("file");
            $upload = $this->upload->data();
            if ($upload) {
                if ($old_img != '') {
                    unlink("./assets/bill_img/" . $old_img);
                }
                $payment[$index]['img'] = $upload['file_name'];
            }
        }

        if($payment[$index]['payment_date'] == ''){
            $payment[$index]['tc_code'] = 'TC-'.$billing[0]['period'].'/'.substr($billing[0]['m_code'], 6).'/'.$index;
            $payment[$index]['course'] = unserialize($this->main_model->gdo4p('m_courses', 'course', 'm_code', $billing[0]['m_code']));;
            $payment[$index]['price'] = $amount;
            $payment[$index]['status'] = 'payed';
            $message = "Melakukan pembayaran bulan {$payment[$index]['month']} untuk member {$billing[0]['m_code']}";
        }else{
            $message = "Memperbarui pembayaran bulan {$payment[$index]['month']} untuk member {$billing[0]['m_code']}";
        }

        $payment[$index]['payment_date'] = $payment_date;

        $data['payment'] = serialize($payment);
        $update = $this->main_model->update('bills', $data, 'id', $id);
        logs($message);
    }

    public function reset()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $id = $obj->id;
        $m_index = to_m($obj->month);
        $billing = $this->main_model->gda3p('bills', 'id', $id);
        $payment = unserialize($billing[0]['payment']);

        $payment[$m_index]['tc_code'] = '';
        $payment[$m_index]['status'] = 'billed';
        $payment[$m_index]['course'] = '';
        $payment[$m_index]['price'] = 0;
        $payment[$m_index]['payment_date'] = '';

        $old_img = $payment[$m_index]['img'];
        if ($old_img != '') {
            unlink("./assets/bill_img/" . $old_img);
            $payment[$m_index]['img'] = '';
        }else{
            $payment[$m_index]['img'] = '';
        }

        $data['payment'] = serialize($payment);
        $update = $this->main_model->update('bills', $data, 'id', $id);
        if($update){
            $message = "Mereset pembayaran bulan {$payment[$m_index]['month']} untuk member {$billing[0]['m_code']}";
            logs($message);
            r_success();
        }

    }

}
