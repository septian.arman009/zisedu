<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth_controller extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('main_model');
        guest();
    }

    public function index()
    {
        $this->load->view('auth/signin');
    }

    public function forgot_password()
    {
        $this->load->view('auth/forgot');
    }

    public function signin()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $email = $obj->email;
        $password = md5($obj->password);

        $user = $this->main_model->gda5p('users', 'email', $email, 'password', $password);
        if ($user) {
            $session_array = array();
            foreach ($user as $key => $val) {
                $session_array = array(
                    'id' => $val['user_id'],
                    'name' => $val['name'],
                    'email' => $val['email'],
                    'role' => $this->main_model->gdo4p('roles', 'name', 'role_id', $val['role_id']),
                );
            }
            $this->session->set_userdata('bim_in', $session_array);
            r_success();
        }
    }

    public function exist_check()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $value = $obj->value;
        $column = $obj->column;
        $table = $obj->table;

        $check = $this->main_model->gda3p($table, $column, $value);
        if ($check) {
            r_success();
        }
    }

    public function send_token()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $email = $obj->email;
        $data['token'] = random_string(40);
        $data['email'] = $email;
        $url = base_url() . 'reset_password/' . $data['token'];
        $message = "Klik to reset password : {$url}";
        $send_mail = send_mail($email, $message, 'Reset Password Zis - EDU');
        if ($send_mail) {
            $insert = $this->main_model->store('token_passwords', $data);
            r_success();
        }

    }

    public function reset_password($token)
    {
        $check = $this->main_model->gda3p('token_passwords', 'token', $token);
        if (!$check) {
            $this->load->view('404');
        } else {
            $data['email'] = $check[0]['email'];
            $this->load->view('auth/reset_password', $data);
        }
    }

    public function save_new_password()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $email = $obj->email;
        $data['password'] = md5($obj->password);

        $reset = $this->main_model->update('users', $data, 'email', $email);
        if ($reset) {
            $this->main_model->destroy('token_passwords', 'email', $email);
            r_success();
        }
    }
}
