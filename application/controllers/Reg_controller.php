<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Reg_controller extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('main_model');
        auth();
    }

    public function registration()
    {
        if (role(['admin','user','owner'], false)) {
            $this->load->view('admin/content/master/member/registration');
        } else {
            $this->load->view('403');
        }
    }

    public function search_member()
    {
        $name = $this->input->get('query');
        $data = $this->db->query("select name from members where name like '%$name%' and status = 1")->result();
        echo json_encode($data);
    }

    public function search_course()
    {
        $name = $this->input->get('query');
        $data = $this->db->query("select name from courses where name like '%$name%'")->result();
        echo json_encode($data);
    }

    public function teacher_list($c, $e, $level)
    {
        $data = '';
        $course = str_replace('-', ' ', $c);
        $edu = str_replace('-', ' ', $e);

        $c_code = $this->main_model->gdo4p('courses', 'c_code', 'name', $course);
        $t_code_list = $this->main_model->gda7p('c_prices', 'c_code', $c_code, 'edu', $edu, 'edu_level', $level);

        $t_code = '';
        foreach ($t_code_list as $key => $value) {
            if ($t_code == '') {
                $t_code = "'" . $value['t_code'] . "'";
            } else {
                $t_code = $t_code . ",'" . $value['t_code'] . "'";
            }
        }

        if ($t_code != '') {
            $teacher = $this->main_model->in3p('teachers', 't_code', $t_code);
            foreach ($teacher as $key => $value) {
                $data .= "<option value='" . $value['t_code'] . "'>" . $value['name'] . "</option>";
            }
        }

        echo $data;
    }

    public function price_check($c, $e, $level, $t_code)
    {
        $data = '';
        $course = str_replace('-', ' ', $c);
        $edu = str_replace('-', ' ', $e);

        $c_code = $this->main_model->gdo4p('courses', 'c_code', 'name', $course);
        $price = $this->main_model->gda9p('c_prices', 'c_code', $c_code, 'edu', $edu, 'edu_level', $level, 't_code', $t_code);
        if ($price) {
            $data = "Course Price : " . torp($price[0]['price']);
        }
        echo $data;
    }

    public function add_m_course()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $course = $obj->course;
        $c_code = $this->main_model->gdo4p('courses', 'c_code', 'name', $obj->course);
        $t_code = $obj->t_code;
        $m_code = $obj->m_code;
        $edu = $obj->edu;
        $level = $obj->level;
        $check = $this->main_model->gda3p('m_courses', 'm_code', $m_code);
        if (!$check) {
            $data['mc_code'] = generate_code('MC', 'm_courses');
            $data['m_code'] = $m_code;
            $m_price = $this->main_model->gda9p('c_prices', 'c_code', $c_code, 't_code', $t_code, 'edu', $edu, 'edu_level', $level);
            if ($m_price) {
                $c_list[] = array(
                    'c_code' => $c_code,
                    'name' => $course,
                    't_code' => $t_code,
                    'edu' => $edu,
                    'level' => $level,
                    'price' => $m_price[0]['price'],
                );
            }
            $data['course'] = serialize($c_list);
            $save = $this->main_model->store('m_courses', $data);
            if ($save) {
                $message = "Menambahkan course list pada member :  {$m_code}";
                logs($message);
                r_success();
            }
        } else {
            $c_list = unserialize($check[0]['course']);
            $check_array = arraySearch($c_list, 'c_code', $c_code);
            $m_price = $this->main_model->gda9p('c_prices', 'c_code', $c_code, 't_code', $t_code, 'edu', $edu, 'edu_level', $level);
            if ($m_price) {
                $new_data = array(
                    'c_code' => $c_code,
                    'name' => $course,
                    't_code' => $t_code,
                    'edu' => $edu,
                    'level' => $level,
                    'price' => $m_price[0]['price'],
                );
                if ($check_array === false) {
                    array_push($c_list, $new_data);
                } else {
                    $c_list[$check_array] = $new_data;
                }
                $data['course'] = serialize($c_list);
                $update = $this->main_model->update('m_courses', $data, 'm_code', $m_code);
                if ($update) {
                    $message = "Memperbarui course list pada member : {$m_code}";
                    logs($message);
                    r_success();
                }
            } else {
                r_error();
            }
        }
    }

    public function course_list($m_code)
    {
        $course = $this->main_model->gda3p('m_courses', 'm_code', $m_code);
        $data = '';
        if (!empty($course[0]['course'])) {
            $c_list = unserialize($course[0]['course']);
            $no = 1;
            $total = 0;
            foreach ($c_list as $key => $value) {
                $data .= "
                <tr>
                    <td>" . $no++ . "</td>
                    <td>" . $value['c_code'] . "</td>
                    <td>" . $value['name'] . "</td>
                    <td>" . $this->main_model->gdo4p('teachers', 'name', 't_code', $value['t_code']) . "</td>
                    <td>" . $value['edu'] . "</td>
                    <td>" . $value['level'] . "</td>
                    <td>" . torp($value['price']) . "</td>
                    <td><a id='destroy' title='Delete' class='btn btn-danger btn-xs waves-effect'
                    onclick='remove_course(" . '"' . $m_code . '"' . ", " . '"' . $value['c_code'] . '"' . ")'><i class='material-icons'>delete</i></a></td>
                </tr>
                ";
                $total += $value['price'];
            }

            if (count($c_list) > 0) {
                $check = $this->main_model->gda5p('bills', 'm_code', $m_code, 'period', date('Y'));
                if (!$check) {
                    $button = "
                        <button id='m_save' onclick='reg_billing(" . '"' . $m_code . '"' . ")'
                            class='btn btn-block bg-pink waves-effect'>Register to Billing</button>
                        <button style='display:none' id='m_loading'
                            class='btn btn-block bg-pink waves-effect'>Loading ..</button>";
                } else {
                    $button = "
                        <button id='m_save' class='btn btn-success bg-pink waves-effect'>Billing Registered</button>";
                }

            } else {
                $button = "
                    <button disabled id='m_save'
                        class='btn btn-block bg-pink waves-effect'>Register to Billing</button>
                    <button style='display:none' id='m_loading'
                        class='btn btn-block bg-pink waves-effect'>Loading ..</button>";
            }

            $data .= "
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td style='font-weight: bold;'>TOTAL</td>
                    <td style='font-weight: bold;'>" . torp($total) . "</td>
                    <td>" . $button . "</td>
                </tr>
                ";
        }

        echo $data;
    }

    public function remove_course()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $c_code = $obj->c_code;
        $m_code = $obj->m_code;

        $c_list = unserialize($this->main_model->gdo4p('m_courses', 'course', 'm_code', $m_code));
        if (count($c_list) > 1) {
            $index = arraySearch($c_list, 'c_code', $c_code);
            unset($c_list[$index]);
            $data['course'] = serialize($c_list);
            $update = $this->main_model->update('m_courses', $data, 'm_code', $m_code);
            if ($update) {
                $message = "Menghapus course : {$c_code} dari course list pada member : {$m_code}";
                logs($message);
                r_success();
            }
        }

    }

    public function reg_billing()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $m_code = $obj->m_code;
        $check = $this->main_model->gda5p('bills', 'm_code', $m_code, 'period', date('Y'));
        if (!$check) {
            $month = array(
                1 => 'January',
                2 => 'February',
                3 => 'March',
                4 => 'April',
                5 => 'May',
                6 => 'June',
                7 => 'July',
                8 => 'August',
                9 => 'September',
                10 => 'October',
                11 => 'November',
                12 => 'December');

            $month_now = array_search(to_month(date('m')), $month);

            $index = 1;
            foreach ($month as $key => $value) {
                if ($index >= $month_now) {
                    $m_list[$index] = array(
                        'index' => $index,
                        'tc_code' => '',
                        'month' => $value,
                        'status' => 'billed',
                        'course' => '',
                        'price' => 0,
                        'payment_date' => '',
                        'img' => '');
                } else {
                    $m_list[$index] = array(
                        'index' => $index,
                        'tc_code' => '',
                        'month' => $value,
                        'status' => 'unbilled',
                        'course' => '',
                        'price' => 0,
                        'payment_date' => '',
                        'img' => '');
                }
                $index++;
            }

            $data['b_code'] = generate_code('BL', 'bills');
            $data['m_code'] = $m_code;
            $data['period'] = date('Y');
            $data['payment'] = serialize($m_list);

            $save = $this->main_model->store('bills', $data);
            if ($save) {
                $message = "Menambahkan billing pada member : {$m_code}";
                logs($message);
                r_success();
            }
        }
    }

    public function renew($m_code, $year)
    {
        $check = $this->main_model->gda5p('bills', 'm_code', $m_code, 'period', $year);
        if (!$check) {
            $month = array(
                1 => 'January',
                2 => 'February',
                3 => 'March',
                4 => 'April',
                5 => 'May',
                6 => 'June',
                7 => 'July',
                8 => 'August',
                9 => 'September',
                10 => 'October',
                11 => 'November',
                12 => 'December');

            $index = 1;
            foreach ($month as $key => $value) {
                $m_list[$index] = array(
                    'index' => $index,
                    'tc_code' => '',
                    'month' => $value,
                    'status' => 'billed',
                    'course' => '',
                    'price' => 0,
                    'payment_date' => '',
                    'img' => '');
                $index++;
            }

            $data['b_code'] = generate_code('BL', 'bills');
            $data['m_code'] = $m_code;
            $data['period'] = $year;
            $data['payment'] = serialize($m_list);

            $save = $this->main_model->store('bills', $data);
            if ($save) {
                $message = "Memperbarui billing untuk tahun {$data['period']} pada member : {$m_code}";
                logs($message);
                r_success();
            }
        }
    }

    public function renew_form($year)
    {
        if (role(['admin', 'user', 'owner'], false)) {
            $data['month'] = array(
                'January',
                'February',
                'March',
                'April',
                'May',
                'June',
                'July',
                'August',
                'September',
                'October',
                'November',
                'December');
            $data['m'] = to_month(date('m'));
            $data['year'] = $year;
            $this->load->view('admin/content/payment/billing/renew', $data);
        } else {
            $this->load->view('403');
        }
    }

    public function renew_billing()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $m_code = $this->main_model->gdo4p('members', 'm_code', 'name', $obj->name);
        $m_index = to_m($obj->month);
        $year = $obj->year;

        $check = $this->main_model->gda5p('bills', 'm_code', $m_code, 'period', $year);
        if (!$check) {
            $month = array(
                1 => 'January',
                2 => 'February',
                3 => 'March',
                4 => 'April',
                5 => 'May',
                6 => 'June',
                7 => 'July',
                8 => 'August',
                9 => 'September',
                10 => 'October',
                11 => 'November',
                12 => 'December');

            $index = 1;
            foreach ($month as $key => $value) {
                if ($index >= $m_index) {
                    $m_list[$index] = array(
                        'index' => $index,
                        'tc_code' => '',
                        'month' => $value,
                        'status' => 'billed',
                        'course' => '',
                        'price' => 0,
                        'payment_date' => '',
                        'img' => '');
                } else {
                    $m_list[$index] = array(
                        'index' => $index,
                        'tc_code' => '',
                        'month' => $value,
                        'status' => 'unbilled',
                        'course' => '',
                        'price' => 0,
                        'payment_date' => '',
                        'img' => '');
                }

                $index++;
            }

            $data['b_code'] = generate_code('BL', 'bills');
            $data['m_code'] = $m_code;
            $data['period'] = $year;
            $data['payment'] = serialize($m_list);

            $save = $this->main_model->store('bills', $data);
            if ($save) {
                $message = "Perpanjangan tagihan untuk tahun {$data['period']} pada member : {$m_code}";
                logs($message);
                r_success();
            }
        }else{
            r_exist();
        }
    }

}
