<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

if (!function_exists('auth')) {
    function auth()
    {
        $ci = &get_instance();
        if (!$ci->session->userdata('bim_in')) {
            redirect('signin');
        }
    }
}

if (!function_exists('guest')) {
    function guest()
    {
        $ci = &get_instance();
        if ($ci->session->userdata('bim_in')) {
            redirect('admin');
        }
    }
}

if (!function_exists('role')) {
    function role($role, $die)
    {
        $ci = &get_instance();
        if (in_array($_SESSION['bim_in']['role'], $role)) {
            return true;
        } else {
            if ($die == true) {
                $status = array('status' => 'errors');
                $ci->mylib->setJSON();
                echo json_encode($status);
                die();
            }
        }
    }
}

if (!function_exists('r_success')) {
    function r_success()
    {
        $ci = &get_instance();
        $status = array('status' => 'success');
        $ci->mylib->setJSON();
        echo json_encode($status);
    }
}

if (!function_exists('r_error')) {
    function r_error()
    {
        $ci = &get_instance();
        $status = array('status' => 'error');
        $ci->mylib->setJSON();
        echo json_encode($status);
    }
}

if (!function_exists('r_exist')) {
    function r_exist()
    {
        $ci = &get_instance();
        $status = array('status' => 'exist');
        $ci->mylib->setJSON();
        echo json_encode($status);
    }
}

if (!function_exists('r_success_data')) {
    function r_success_data($data)
    {
        $ci = &get_instance();
        $status = array('status' => 'success', 'data' => $data);
        $ci->mylib->setJSON();
        echo json_encode($status);
    }
}

if (!function_exists('generate_code')) {
    function generate_code($front_code, $table)
    {
        $ci = &get_instance();
        $ci->load->model('main_model');
        $last_number = $ci->main_model->last_ai($table);

        if ($last_number < 10) {
            $last_number = '000' . $last_number;
        } else if ($last_number < 100) {
            $last_number = '00' . $last_number;
        } else if ($last_number < 1000) {
            $last_number = '0' . $last_number;
        } else {
            $last_number = $last_number;
        }

        $date = explode('-', date('y-m-d'));
        $code = $front_code . '-' . $date['1'] . '' . $date['0'] . $last_number;
        return $code;
    }
}

if (!function_exists('logs')) {
    function logs($message)
    {
        $ci = &get_instance();
        $ci->load->model('main_model');
        $log['user_id'] = $_SESSION['bim_in']['id'];
        $log['name'] = $_SESSION['bim_in']['name'];
        $log['log'] = $message;
        $ci->main_model->store('logs', $log);
    }
}

if (!function_exists('do_action')) {
    function do_action($id, $table, $column, $data, $message)
    {
        $ci = &get_instance();
        $ci->load->model('main_model');
        if ($id == 'null') {
            $store = $ci->main_model->store($table, $data);
            if ($store) {
                logs($message);
                r_success();
            }
        } else {
            $update = $ci->main_model->update($table, $data, $column, $id);
            if ($update) {
                logs($message);
                r_success();
            }
        }
    }
}

if (!function_exists('show_table')) {
    function show_table($id, $table, $column, $param)
    {
        $ci = &get_instance();
        $ci->load->model('d_table');
        if (
            isset($_SERVER['HTTP_X_REQUESTED_WITH']) &&
            !empty($_SERVER['HTTP_X_REQUESTED_WITH']) &&
            strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
        ) {
            $datatables = $_POST;
            $datatables['table'] = $table;
            $datatables['id-table'] = $id;
            $datatables['param'] = $param;
            $datatables['col-display'] = $column;
            $ci->d_table->Datatables($datatables);
        }
        return;
    }
}

if (!function_exists('send_mail')) {
    function send_mail($email, $message, $subject)
    {
        $ci = &get_instance();
        $ci->load->model('main_model');
        $setting = $ci->main_model->gda1p('settings');
        $ci->load->library('email');
        $config = array(
            'protocol' => $setting[0]['protocol'],
            'smtp_host' => $setting[0]['smtp_host'],
            'smtp_port' => $setting[0]['smtp_port'],
            'smtp_user' => $setting[0]['send_mail'],
            'smtp_pass' => $setting[0]['send_pass'],
            'mailtype' => 'html',
            'charset' => 'utf-8',
        );
        $ci->email->initialize($config);
        $ci->email->set_mailtype("html");
        $ci->email->set_newline("\r\n");
        $ci->email->to($email);
        $ci->email->from($setting[0]['send_mail'], 'Zis - EDU');
        $ci->email->subject($subject);
        $ci->email->message($message);
        $status = $ci->email->send();
        return $status;
    }
}

if (!function_exists('level')) {
    function level($edu, $level)
    {
        if($edu == 'General'){
            return 'General';
        }else if($edu == 'Primary School' || $edu == 'Junior High School' || $edu == 'Senior High School'){
            return 'Class '.$level;
        }else{
            return 'Semester '.$level;
        }
    }
}

//Support Function -->

if (!function_exists('torp')) {
    function torp($number)
    {
        if ($number >= 0) {
            return 'Rp. ' . strrev(implode('.', str_split(strrev(strval($number)), 3)));
        } else {
            $num = explode('-', $number);
            return 'Rp. - ' . strrev(implode('.', str_split(strrev(strval($num[1])), 3)));
        }
    }
}

if (!function_exists('array_key_last')) {
    function array_key_last($array)
    {
        $key = null;
        if (is_array($array)) {
            end($array);
            $key = key($array);
        }
        return $key;
    }
}

if (!function_exists('arraySearch')) {
    function arraySearch($array, $field, $search)
    {
        foreach ($array as $key => $value) {
            if ($value[$field] === $search) {
                return $key;
            }
        }
        return false;
    }
}

if (!function_exists('to_indo_day')) {
    function to_indo_day($day)
    {
        if ($day == 'Monday') {
            return 'Senin';
        } else if ($day == 'Tuesday') {
            return 'Selasa';
        } else if ($day == 'Wednesday') {
            return 'Rabu';
        } else if ($day == 'Thursday') {
            return 'Kamis';
        } else if ($day == 'Friday') {
            return 'Jumat';
        } else if ($day == 'Saturday') {
            return 'Sabtu';
        } else if ($day == 'Sunday') {
            return 'Minggu';
        }
    }
}

if (!function_exists('to_dump')) {
    function to_dump($data)
    {
        echo '<pre>';
        var_dump($data);
        die();
        echo '</pre>';
    }
}

if (!function_exists('random_string')) {
    function random_string($length)
    {
        $chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
        $string = '';
        for ($i = 0; $i < $length; $i++) {
            $string .= $chars[rand(0, strlen($chars) - 1)];

        }
        return $string;
    }
}

if (!function_exists('to_int')) {
    function to_int($price)
    {
        $rep1 = str_replace('Rp. ', '', $price);
        $rep2 = str_replace('.', '', $rep1);
        return $rep2;
    }
}

if (!function_exists('to_date_time')) {
    function to_date_time($date)
    {
        $bersih = explode(' ', $date);
        $pisah = explode('-', $bersih[0]);
        if ($pisah[1] == '01') {
            $pisah[1] = 'Januari';
        } else if ($pisah[1] == '02') {
            $pisah[1] = 'Februari';
        } else if ($pisah[1] == '03') {
            $pisah[1] = 'Maret';
        } else if ($pisah[1] == '04') {
            $pisah[1] = 'April';
        } else if ($pisah[1] == '05') {
            $pisah[1] = 'Mei';
        } else if ($pisah[1] == '06') {
            $pisah[1] = 'Juni';
        } else if ($pisah[1] == '07') {
            $pisah[1] = 'Juli';
        } else if ($pisah[1] == '08') {
            $pisah[1] = 'Agustus';
        } else if ($pisah[1] == '09') {
            $pisah[1] = 'September';
        } else if ($pisah[1] == '10') {
            $pisah[1] = 'Oktober';
        } else if ($pisah[1] == '11') {
            $pisah[1] = 'November';
        } else if ($pisah[1] == '12') {
            $pisah[1] = 'Desember';
        }
        $urutan = array($pisah[2], $pisah[1], $pisah[0]);
        $satukan = implode(' ', $urutan);
        return $satukan . ' ' . $bersih[1];
    }
}

if (!function_exists('to_date')) {
    function to_date($date)
    {
        $pisah = explode('-', $date);
        if ($pisah[1] == '01') {
            $pisah[1] = 'January';
        } else if ($pisah[1] == '02') {
            $pisah[1] = 'February';
        } else if ($pisah[1] == '03') {
            $pisah[1] = 'March';
        } else if ($pisah[1] == '04') {
            $pisah[1] = 'April';
        } else if ($pisah[1] == '05') {
            $pisah[1] = 'May';
        } else if ($pisah[1] == '06') {
            $pisah[1] = 'June';
        } else if ($pisah[1] == '07') {
            $pisah[1] = 'July';
        } else if ($pisah[1] == '08') {
            $pisah[1] = 'August';
        } else if ($pisah[1] == '09') {
            $pisah[1] = 'September';
        } else if ($pisah[1] == '10') {
            $pisah[1] = 'October';
        } else if ($pisah[1] == '11') {
            $pisah[1] = 'November';
        } else if ($pisah[1] == '12') {
            $pisah[1] = 'December';
        }
        $urutan = array($pisah[2], $pisah[1], $pisah[0]);
        $satukan = implode(' ', $urutan);
        return $satukan;
    }
}

if (!function_exists('to_date_mysql')) {
    function to_date_mysql($date)
    {
        $pisah = explode(' ', $date);
        if ($pisah[1] == 'January') {
            $pisah[1] = '01';
        } else if ($pisah[1] == 'February') {
            $pisah[1] = '02';
        } else if ($pisah[1] == 'March') {
            $pisah[1] = '03';
        } else if ($pisah[1] == 'April') {
            $pisah[1] = '04';
        } else if ($pisah[1] == 'May') {
            $pisah[1] = '05';
        } else if ($pisah[1] == 'June') {
            $pisah[1] = '06';
        } else if ($pisah[1] == 'July') {
            $pisah[1] = '07';
        } else if ($pisah[1] == 'August') {
            $pisah[1] = '08';
        } else if ($pisah[1] == 'September') {
            $pisah[1] = '09';
        } else if ($pisah[1] == 'October') {
            $pisah[1] = '10';
        } else if ($pisah[1] == 'November') {
            $pisah[1] = '11';
        } else if ($pisah[1] == 'Deember') {
            $pisah[1] = '12';
        }
        $urutan = array($pisah[2], $pisah[1], $pisah[0]);
        $satukan = implode('-', $urutan);
        return $satukan;
    }
}

if (!function_exists('to_month')) {
    function to_month($month)
    {
        if ($month == '01') {
            $month = 'January';
        } else if ($month == '02') {
            $month = 'February';
        } else if ($month == '03') {
            $month = 'March';
        } else if ($month == '04') {
            $month = 'April';
        } else if ($month == '05') {
            $month = 'May';
        } else if ($month == '06') {
            $month = 'June';
        } else if ($month == '07') {
            $month = 'July';
        } else if ($month == '08') {
            $month = 'August';
        } else if ($month == '09') {
            $month = 'September';
        } else if ($month == '10') {
            $month = 'October';
        } else if ($month == '11') {
            $month = 'November';
        } else if ($month == '12') {
            $month = 'December';
        }
        return $month;
    }
}

if (!function_exists('to_m')) {
    function to_m($month)
    {
        if ($month == 'January') {
            $month = '1';
        } else if ($month == 'February') {
            $month = '2';
        } else if ($month == 'March') {
            $month = '3';
        } else if ($month == 'April') {
            $month = '4';
        } else if ($month == 'May') {
            $month = '5';
        } else if ($month == 'June') {
            $month = '6';
        } else if ($month == 'July') {
            $month = '7';
        } else if ($month == 'August') {
            $month = '8';
        } else if ($month == 'September') {
            $month = '9';
        } else if ($month == 'October') {
            $month = '10';
        } else if ($month == 'November') {
            $month = '11';
        } else if ($month == 'December') {
            $month = '12';
        }
        return $month;
    }
}
